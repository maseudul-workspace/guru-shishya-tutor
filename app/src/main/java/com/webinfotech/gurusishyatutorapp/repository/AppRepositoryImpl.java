package com.webinfotech.gurusishyatutorapp.repository;

import com.google.gson.Gson;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsWrapper;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingListWrapper;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.domain.models.JobDetailsWrapper;
import com.webinfotech.gurusishyatutorapp.domain.models.OrderPlaceResponse;
import com.webinfotech.gurusishyatutorapp.domain.models.TutionListWrapper;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfoWrapper;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryWrapper;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public UserInfoWrapper checkLogin(String phone, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(phone, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserInfoWrapper registerUser(String name,
                                       String mobile,
                                       String password,
                                       String confirmPassword
    ) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerUser(name, mobile, password, confirmPassword);
            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public CommonResponse sendOtp(String phone) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendOtp(phone);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse verifyOtp(String phone, String otp) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.verifyOtp(otp, phone);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public JobDetailsWrapper fetchJobDetails(String apiToken, int tutorId) {
        JobDetailsWrapper jobDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchJobDetails("Bearer " + apiToken, tutorId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    jobDetailsWrapper = null;
                }else{
                    jobDetailsWrapper = gson.fromJson(responseBody, JobDetailsWrapper.class);
                }
            } else {
                jobDetailsWrapper = null;
            }
        }catch (Exception e){
            jobDetailsWrapper = null;
        }
        return jobDetailsWrapper;
    }

    public CommonResponse addJob(String apiToken,
                                 int userId,
                                 int classId,
                                 int subjectId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addJob("Bearer " + apiToken, userId, classId, subjectId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateProfile(String apiToken,
                                        int userId,
                                        String state,
                                        String city,
                                        String pin,
                                        double latitude,
                                        double longitude,
                                        String address,
                                        String qualification,
                                        String experience,
                                        String youtubeLink,
                                        String document,
                                        String certificate,
                                        String profileImage
    ) {
        RequestBody userIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(userId));
        RequestBody stateRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, state);
        RequestBody cityRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, city);
        RequestBody pinRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, pin);
        RequestBody latitudeRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Double.toString(latitude));
        RequestBody longitudeRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Double.toString(longitude));

        RequestBody addressRequestBody = null;
        if (address != null) {
            addressRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, address);
        }

        RequestBody qualificationRequestBody = null;
        if (qualification != null) {
            qualificationRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, qualification);
        }

        RequestBody experienceRequestBody = null;
        if (experience != null) {
            experienceRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, experience);
        }

        RequestBody youtubeLinkRequestBody = null;
        if (youtubeLink != null) {
            youtubeLinkRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, youtubeLink);
        }

        MultipartBody.Part documentFileBody = null;
        if (document != null) {
            File imageFile = new File(document);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            documentFileBody = MultipartBody.Part.createFormData("document", imageFile.getName(), requestImageFile);
        }

        MultipartBody.Part certificateFileBody = null;
        if (certificate != null) {
            File imageFile = new File(certificate);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            certificateFileBody = MultipartBody.Part.createFormData("certificate", imageFile.getName(), requestImageFile);
        }

        MultipartBody.Part profileImageFileBody = null;
        if (profileImage != null) {
            File imageFile = new File(profileImage);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            profileImageFileBody = MultipartBody.Part.createFormData("profile_image", imageFile.getName(), requestImageFile);
        }

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateProfile("Bearer " + apiToken, userIdRequestBody, stateRequestBody, cityRequestBody, pinRequestBody, latitudeRequestBody, longitudeRequestBody, addressRequestBody, qualificationRequestBody, experienceRequestBody, youtubeLinkRequestBody, documentFileBody, certificateFileBody, profileImageFileBody);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper fetchUserDetails(String apiToken, int userId) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserDetails("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public OrderPlaceResponse placeOrder(String apiToken, int userId) {

        OrderPlaceResponse orderPlaceResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> place = mRepository.placeOrder("Bearer " + apiToken, userId);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceResponse = null;
                }else{
                    orderPlaceResponse = gson.fromJson(responseBody, OrderPlaceResponse.class);
                }
            } else {
                orderPlaceResponse = null;
            }
        }catch (Exception e){
            orderPlaceResponse = null;
        }
        return orderPlaceResponse;
    }

    public CommonResponse verifyPayment( String authorization,
                                         int userId,
                                         String razorpayOrderId,
                                         String razorpayPaymentId,
                                         String razorpaySignature) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.verifyPayment("Bearer " + authorization, userId, razorpayOrderId, razorpayPaymentId, razorpaySignature);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse removeTutorSubject(String apiToken, int tutorSubjectId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeTutorSubject("Bearer " + apiToken, tutorSubjectId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse removeTutorClass(String apiToken, int tutorClassId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeTutorClass("Bearer " + apiToken, tutorClassId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse removeTimeSlot(String apiToken, int timeSlotId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeTimeSlot("Bearer " + apiToken, timeSlotId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse addTimeSlot(String apiToken,
                                      int userId,
                                      String timeSlotFrom,
                                      String timeSlotTo) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addTimeSlot("Bearer " + apiToken, userId, timeSlotFrom, timeSlotTo);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse sendForgetPasswordOtp(String phone) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.forgetPasswordSendOtp(phone);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse forgetPassword(String otp,
                                        String mobile,
                                        String password,
                                        String confirmPassword
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.forgetPassword(otp, mobile, password, confirmPassword);
            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse verifyForgetPasswordOtp(String phone, String otp) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.forgetPasswordOtpVerify(phone, otp);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public BookingListWrapper fetchBookingList(String apiToken, int page) {
        BookingListWrapper bookingListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchBookingList("Bearer " + apiToken, page);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    bookingListWrapper = null;
                }else{
                    bookingListWrapper = gson.fromJson(responseBody, BookingListWrapper.class);
                }
            } else {
                bookingListWrapper = null;
            }
        }catch (Exception e){
            bookingListWrapper = null;
        }
        return bookingListWrapper;
    }

    public BookingDetailsWrapper fetchBookingDetails(String apiToken, int order_id) {
        BookingDetailsWrapper bookingDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchBookingDetails("Bearer " + apiToken, order_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    bookingDetailsWrapper = null;
                }else{
                    bookingDetailsWrapper = gson.fromJson(responseBody, BookingDetailsWrapper.class);
                }
            } else {
                bookingDetailsWrapper = null;
            }
        }catch (Exception e){
            bookingDetailsWrapper = null;
        }
        return bookingDetailsWrapper;
    }

    public CommonResponse acceptBooking(String apiToken, int order_id) {

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.acceptBooking("Bearer " + apiToken, order_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse rejectBooking(String apiToken, int order_id) {

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.rejectBooking("Bearer " + apiToken, order_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse passwordChange(String apiToken,int tutor_id, String current_password, String new_password, String confirm_password) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.passwordChange("Bearer " + apiToken, tutor_id, current_password, new_password, confirm_password);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse withdrawRequest(String apiToken, String amount, int tutor_id) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.withdrawRequest("Bearer " + apiToken, amount, tutor_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public WalletHistoryWrapper fetchWalletHistory(String apiToken, int tutor_id) {
        WalletHistoryWrapper walletHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.walletHistory("Bearer " + apiToken, tutor_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletHistoryWrapper = null;
                }else{
                    walletHistoryWrapper = gson.fromJson(responseBody, WalletHistoryWrapper.class);
                }
            } else {
                walletHistoryWrapper = null;
            }
        }catch (Exception e){
            walletHistoryWrapper = null;
        }
        return walletHistoryWrapper;
    }

    public TutionListWrapper fetchTutionList(String apiToken, int page) {
        TutionListWrapper tutionListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchTutionList("Bearer " + apiToken, page);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    tutionListWrapper = null;
                }else{
                    tutionListWrapper = gson.fromJson(responseBody, TutionListWrapper.class);
                }
            } else {
                tutionListWrapper = null;
            }
        }catch (Exception e){
            tutionListWrapper = null;
        }
        return tutionListWrapper;
    }

    public CommonResponse tutionAttendance(String apiToken, int booking_details_id) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.tutionAttendance("Bearer " + apiToken, booking_details_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse tutionComplete(String apiToken, int tution_id) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.tutionComplete("Bearer " + apiToken, tution_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse sendAppId(String apiToken, String app_id) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.sendAppId("Bearer " + apiToken,app_id);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

}
