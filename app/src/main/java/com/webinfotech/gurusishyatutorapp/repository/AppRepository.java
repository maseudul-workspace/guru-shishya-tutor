package com.webinfotech.gurusishyatutorapp.repository;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AppRepository {

    @POST("tutor/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("mobile") String mobile,
                                  @Field("password") String password
    );

    @POST("tutor/register")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("name") String name,
                                    @Field("mobile") String mobile,
                                    @Field("password") String password,
                                    @Field("confirm_password") String confirmPassword
    );

    @GET("tutor/send/signup/otp/{mobile}")
    Call<ResponseBody> sendOtp(@Path("mobile") String mobile);

    @POST("tutor/signup/otp/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyOtp(@Field("otp") String otp,
                                 @Field("mobile") String mobile
    );

    @GET("tutor/job/detail/fetch/{tutor_id}")
    Call<ResponseBody> fetchJobDetails(@Header("Authorization") String authorization,
                                       @Path("tutor_id") int tutorId
    );

    @POST("tutor/job/update")
    @FormUrlEncoded
    Call<ResponseBody> addJob(@Header("Authorization") String authorization,
                              @Field("user_id") int userId,
                              @Field("class[0]") int classId,
                              @Field("subject[0]") int subjectId
    );

    @POST("tutor/profile/update")
    @Multipart
    Call<ResponseBody> updateProfile( @Header("Authorization") String authorization,
                                      @Part("user_id") RequestBody userId,
                                      @Part("state") RequestBody state,
                                      @Part("city") RequestBody city,
                                      @Part("pin") RequestBody pin,
                                      @Part("lat") RequestBody latitude,
                                      @Part("long") RequestBody longitude,
                                      @Part("address") RequestBody address,
                                      @Part("qualification") RequestBody qualification,
                                      @Part("experience") RequestBody experience,
                                      @Part("youtube_link") RequestBody youtubeLink,
                                      @Part MultipartBody.Part document,
                                      @Part MultipartBody.Part certificate,
                                      @Part MultipartBody.Part profileImage
    );

    @GET("tutor/profile/fetch/{tutor_id}")
    Call<ResponseBody> fetchUserDetails(@Header("Authorization") String authorization,
                                        @Path("tutor_id") int tutorId
    );

    @POST("tutor/activate/pay")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Header("Authorization") String authorization,
                                  @Field("tutor_id") int tutorId
    );

    @POST("tutor/payment/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyPayment(   @Header("Authorization") String authorization,
                                        @Field("tutor_id") int userId,
                                        @Field("razorpay_order_id") String razorpayOrderId,
                                        @Field("razorpay_payment_id") String razorpayPaymentId,
                                        @Field("razorpay_signature") String razorpaySignature
    );

    @GET("tutor/subject/remove/{tutor_subject_id}")
    Call<ResponseBody> removeTutorSubject(  @Header("Authorization") String authorization,
                                            @Path("tutor_subject_id") int tutorSubjectId
    );

    @GET("tutor/class/remove/{tutor_class_id}")
    Call<ResponseBody> removeTutorClass(  @Header("Authorization") String authorization,
                                          @Path("tutor_class_id") int tutorClassId
    );

    @GET("tutor/time/slot/remove/{tutor_time_slot_id}")
    Call<ResponseBody> removeTimeSlot(  @Header("Authorization") String authorization,
                                        @Path("tutor_time_slot_id") int timeSlotId
    );

    @POST("tutor/time_slot/update")
    @FormUrlEncoded
    Call<ResponseBody> addTimeSlot( @Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("time_slot_from[0]") String timeSlotFrom,
                                    @Field("time_slot_to[0]") String timeSlotTo
    );

    @GET("tutor/forget/password/otp/send/{mobile}")
    Call<ResponseBody> forgetPasswordSendOtp(@Path("mobile") String mobile);

    @POST("tutor/forget/password/change")
    @FormUrlEncoded
    Call<ResponseBody> forgetPassword(@Field("otp") String otp,
                                        @Field("mobiile") String mobile,
                                        @Field("new_password") String password,
                                        @Field("confirm_password") String confirmPassword
    );

    @GET("tutor/forget/password/otp/verify/{mobile}/{otp}")
    Call<ResponseBody> forgetPasswordOtpVerify(@Path("mobile") String mobile,
                                               @Path("otp") String otp
                                               );

    @GET("tutor/booking/list")
    Call<ResponseBody> fetchBookingList( @Header("Authorization") String authorization,
                                         @Query("page") int page
    );

    @GET("tutor/tution/details/{tution_id}")
    Call<ResponseBody> fetchBookingDetails( @Header("Authorization") String authorization,
                                            @Path("tution_id") int tution_id
    );

    @GET("tutor/booking/accept/{booking_id}")
    Call<ResponseBody> acceptBooking( @Header("Authorization") String authorization,
                                      @Path("booking_id") int booking_id
    );

    @GET("tutor/booking/reject/{booking_id}")
    Call<ResponseBody> rejectBooking( @Header("Authorization") String authorization,
                                      @Path("booking_id") int booking_id
    );

    @POST("tutor/password/change")
    @FormUrlEncoded
    Call<ResponseBody> passwordChange(  @Header("Authorization") String authorization,
                                         @Field("tutor_id") int tutor_id,
                                         @Field("current_password") String current_password,
                                         @Field("new_password") String new_password,
                                         @Field("confirm_password") String confirm_password

    );

    @POST("tutor/withdraw/request")
    @FormUrlEncoded
    Call<ResponseBody> withdrawRequest(  @Header("Authorization") String authorization,
                                        @Field("amount") String amount,
                                        @Field("tutor_id") int tutor_id

    );

    @GET("tutor/wallet/history/{tutor_id}")
    Call<ResponseBody> walletHistory(    @Header("Authorization") String authorization,
                                         @Path("tutor_id") int tutor_id

    );

    @GET("tutor/tution/list")
    Call<ResponseBody> fetchTutionList( @Header("Authorization") String authorization,
                                         @Query("page") int page
    );

    @GET("tutor/tution/attendance/{tution_details_id}")
    Call<ResponseBody> tutionAttendance( @Header("Authorization") String authorization,
                                         @Path("tution_details_id") int booking_details_id
    );

    @GET("tutor/tution/complete/{tution_id}")
    Call<ResponseBody> tutionComplete( @Header("Authorization") String authorization,
                                         @Path("tution_id") int tuion_id
    );

    @POST("tutor/update/app_id")
    @FormUrlEncoded
    Call<ResponseBody> sendAppId(      @Header("Authorization") String authorization,
                                       @Field("app_id") String app_id
    );

}
