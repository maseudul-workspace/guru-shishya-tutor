package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobDetails {

    @SerializedName("classes")
    @Expose
    public Class[] classes;

    @SerializedName("tutor_class")
    @Expose
    public TutorClass[] tutorClasses;

    @SerializedName("time_slots")
    @Expose
    public TimeSlot[] timeSlots;

}
