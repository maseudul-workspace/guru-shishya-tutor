package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface TutionAttendanceInteractor {
    interface Callback {
        void onTutionAttendanceSuccess();
        void onTutionAttendanceFail(String errorMsg);
    }
}
