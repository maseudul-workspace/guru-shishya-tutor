package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TutionList {

    @SerializedName("order_id")
    @Expose
    public int order_id;

    @SerializedName("student_name")
    @Expose
    public String student_name;

    @SerializedName("class_name")
    @Expose
    public String class_name;

    @SerializedName("booking_date")
    @Expose
    public String booking_date;

    @SerializedName("payment_status")
    @Expose
    public String payment_status;

    @SerializedName("order_status")
    @Expose
    public String order_status;

}
