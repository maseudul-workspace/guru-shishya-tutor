package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeSlot {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("start_time")
    @Expose
    public String startTime;

    @SerializedName("end_time")
    @Expose
    public String endTime;

}
