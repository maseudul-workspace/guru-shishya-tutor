package com.webinfotech.gurusishyatutorapp.domain.models;

/**
 * Created by Raj on 29-06-2019.
 */

public class NotificationModel {

    public String title;
    public String desc;

    public NotificationModel(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }
}
