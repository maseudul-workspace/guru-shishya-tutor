package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;


import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.SendAppIdInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class SendAppIdInteractorImpl extends AbstractInteractor implements SendAppIdInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String app_id;

    public SendAppIdInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, String app_id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.app_id = app_id;
    }

    private void notifyError(final String errorMsg, int login_error) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendAppIdFail(errorMsg, login_error);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendAppIdSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.sendAppId(authorization,app_id);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message,commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
