package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("qualification")
    @Expose
    public String qualification;

    @SerializedName("lat")
    @Expose
    public double latitude;

    @SerializedName("long")
    @Expose
    public double longitude;

    @SerializedName("youtube_link")
    @Expose
    public String youtubeLink;

    @SerializedName("experience")
    @Expose
    public String experience;

    @SerializedName("document")
    @Expose
    public String document;

    @SerializedName("certificate")
    @Expose
    public String certificate;

    @SerializedName("profile_image")
    @Expose
    public String profileImage;

}
