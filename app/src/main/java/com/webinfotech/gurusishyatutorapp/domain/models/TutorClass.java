package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TutorClass {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("class_id")
    @Expose
    public int classId;

    @SerializedName("class_name")
    @Expose
    public String name;

    @SerializedName("is_stream")
    @Expose
    public int isStream;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("subjects")
    @Expose
    public TutorSubject[] subjects;

}
