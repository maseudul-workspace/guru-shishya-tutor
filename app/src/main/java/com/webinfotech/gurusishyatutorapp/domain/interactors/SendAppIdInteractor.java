package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface SendAppIdInteractor {
    interface Callback {
        void onSendAppIdSuccess();
        void onSendAppIdFail(String errorMsg, int loginError);
    }
}
