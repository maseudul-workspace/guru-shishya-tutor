package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface SendForgetPasswordOtpInteractor {
    interface Callback {
        void onOtpSendSuccess();
        void onOtpSendFail(String errorMsg);
    }
}
