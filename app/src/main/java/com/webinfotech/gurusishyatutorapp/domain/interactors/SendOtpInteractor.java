package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface SendOtpInteractor {
    interface Callback {
        void onSendOtpSuccess();
        void onSendOtpFail(String errorMsg);
    }
}
