package com.webinfotech.gurusishyatutorapp.domain.interactors;

import com.webinfotech.gurusishyatutorapp.domain.models.PaymentDataMain;

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess(PaymentDataMain paymentDataMain);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
