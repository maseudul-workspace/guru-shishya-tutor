package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface ForgetPassowordInteractor {
    interface Callback {
        void onForgetPasswordSuccess();
        void pnForgetPasswordFail(String errorMsg);
    }

}
