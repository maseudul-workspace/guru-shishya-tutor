package com.webinfotech.gurusishyatutorapp.domain.interactors;

import com.webinfotech.gurusishyatutorapp.domain.models.TutionList;

public interface TutionListInteractor {
    interface Callback {
        void onGettingTutionListSuccess(TutionList[] tutionLists, int totalPage);
        void onGettingTutionListFail(String errorMsg);
    }
}
