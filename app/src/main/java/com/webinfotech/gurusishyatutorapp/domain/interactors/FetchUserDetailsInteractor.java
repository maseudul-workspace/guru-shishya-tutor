package com.webinfotech.gurusishyatutorapp.domain.interactors;

import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;

public interface FetchUserDetailsInteractor {

    interface Callback {
        void onGettingUserDetailsSuccess(UserInfo userInfo);
        void onGettingUserDetailsFail(String errorMsg, int loginError);
    }

}
