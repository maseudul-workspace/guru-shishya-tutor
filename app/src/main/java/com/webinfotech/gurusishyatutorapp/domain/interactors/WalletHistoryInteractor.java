package com.webinfotech.gurusishyatutorapp.domain.interactors;


import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;

public interface WalletHistoryInteractor {
    interface Callback {
        void onGettingWalletHistorySuccess(WalletHistoryData walletHistoryData);
        void onGettingWalletHistoryFail(String errorMsg);
    }
}
