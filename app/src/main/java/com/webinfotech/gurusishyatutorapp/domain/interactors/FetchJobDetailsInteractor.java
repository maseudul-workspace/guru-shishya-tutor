package com.webinfotech.gurusishyatutorapp.domain.interactors;

import com.webinfotech.gurusishyatutorapp.domain.models.JobDetails;

public interface FetchJobDetailsInteractor {
    interface Callback {
        void onGettingJobDetailsSuccess(JobDetails jobDetails );
        void onGettingJobDetailsFail(String errorMsg, int loginError);
    }
}
