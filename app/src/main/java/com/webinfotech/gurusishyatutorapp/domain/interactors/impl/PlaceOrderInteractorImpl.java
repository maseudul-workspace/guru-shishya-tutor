package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.OrderPlaceResponse;
import com.webinfotech.gurusishyatutorapp.domain.models.PaymentDataMain;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(PaymentDataMain paymentDataMain){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess(paymentDataMain);
            }
        });
    }

    @Override
    public void run() {
        final OrderPlaceResponse orderPlaceResponse = mRepository.placeOrder(apiToken, userId);
        if (orderPlaceResponse == null) {
            notifyError("Please Internet Connection", 0);
        } else if (!orderPlaceResponse.status) {
            notifyError(orderPlaceResponse.message, orderPlaceResponse.login_error);
        } else {
            postMessage(orderPlaceResponse.paymentDataMain);
        }
    }
}
