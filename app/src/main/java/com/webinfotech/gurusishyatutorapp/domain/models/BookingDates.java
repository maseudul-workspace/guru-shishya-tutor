package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingDates {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("time_slot_id")
    @Expose
    public int time_slot_id;

    @SerializedName("time_from")
    @Expose
    public String time_from;

    @SerializedName("time_to")
    @Expose
    public String time_to;

    @SerializedName("is_attend_student")
    @Expose
    public String is_attend_student;

    @SerializedName("is_attend_tutor")
    @Expose
    public String is_attend_tutor;

}
