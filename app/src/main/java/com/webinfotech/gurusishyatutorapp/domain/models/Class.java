package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Class {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("is_stream")
    @Expose
    public int isStream;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("subjects")
    @Expose
    public Subject[] subjects;

}
