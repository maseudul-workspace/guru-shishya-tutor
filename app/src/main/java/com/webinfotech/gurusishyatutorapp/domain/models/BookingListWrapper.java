package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingListWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("current_page")
    @Expose
    public int current_page;

    @SerializedName("total_pages")
    @Expose
    public int total_pages;

    @SerializedName("has_more_pages")
    @Expose
    public boolean has_more_pages;

    @SerializedName("total_data")
    @Expose
    public int total_data;

    @SerializedName("data")
    @Expose
    public BookingList[] bookingList;
}
