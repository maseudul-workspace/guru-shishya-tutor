package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.PasswordChangeInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class PasswordChangeInteractorImpl extends AbstractInteractor implements PasswordChangeInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int tutor_id;
    String current_password;
    String confirm_password;
    String new_password;

    public PasswordChangeInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int tutor_id, String current_password, String new_password, String confirm_password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.tutor_id = tutor_id;
        this.current_password = current_password;
        this.confirm_password = confirm_password;
        this.new_password = new_password;
    }

    private void notifyError(final String errorMsg, int login_error) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPasswordChangedFail(errorMsg, login_error);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPasswordChangedSuccess();
            }
        });
    }


    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.passwordChange(authorization, tutor_id, current_password, new_password , confirm_password);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection",0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message,commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
