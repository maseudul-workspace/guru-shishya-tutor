package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;


import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.TutionCompleteInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class TutionCompleteInteractorImpl extends AbstractInteractor implements TutionCompleteInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int tution_id;

    public TutionCompleteInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int tution_id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.tution_id = tution_id;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onTutionCompleteFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onTutionCompleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.tutionComplete(authorization, tution_id);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
