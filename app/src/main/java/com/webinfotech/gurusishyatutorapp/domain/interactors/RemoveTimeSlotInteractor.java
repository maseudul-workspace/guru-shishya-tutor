package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface RemoveTimeSlotInteractor {
    interface Callback {
        void onTimeSlotDeleteSuccess();
        void onTimeSlotDeleteFailed(String errorMsg, int loginError);
    }
}
