package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface AcceptBookingInteractor {
    interface Callback {
        void onAcceptBookingSuccess();
        void onAcceptBookingFail(String errorMsg);
    }
}
