package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchUserDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfoWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class FetchUserDetailsInteractorImpl extends AbstractInteractor implements FetchUserDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchUserDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.fetchUserDetails(apiToken, userId);
        if (userInfoWrapper == null) {
            notifyError("Please check your internet connection", 0);
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message, userInfoWrapper.login_error);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
