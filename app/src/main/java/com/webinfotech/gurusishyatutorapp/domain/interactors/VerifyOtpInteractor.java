package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface VerifyOtpInteractor {
    interface Callback {
        void onOtpVerifySuccess();
        void onOtpVerifyFail(String errorMsg);
    }
}
