package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchJobDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.JobDetails;
import com.webinfotech.gurusishyatutorapp.domain.models.JobDetailsWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class FetchJobDetailsInteractorImpl extends AbstractInteractor implements FetchJobDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int tutorId;

    public FetchJobDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int tutorId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.tutorId = tutorId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(JobDetails jobDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingJobDetailsSuccess(jobDetails);
            }
        });
    }

    @Override
    public void run() {
        final JobDetailsWrapper jobDetailsWrapper = mRepository.fetchJobDetails(apiToken, tutorId);
        if (jobDetailsWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!jobDetailsWrapper.status) {
            notifyError(jobDetailsWrapper.message, jobDetailsWrapper.login_error);
        } else {
            postMessage(jobDetailsWrapper.jobDetails);
        }
    }
}
