package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface VerifyPaymentInteractor {
    interface Callback {
        void onVerifyPaymentSuccess();
        void onVerifyPaymentFail(String errorMsg);
    }
}
