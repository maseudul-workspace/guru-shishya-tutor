package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface RejectBookingInteractor {
    interface Callback {
        void onRejectBookingSuccess();
        void onRejectBookingFail(String errorMsg);
    }
}
