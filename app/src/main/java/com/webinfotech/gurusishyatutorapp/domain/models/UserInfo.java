package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-02-2019.
 */

public class UserInfo {

    @SerializedName("id")
    @Expose
    public int userId;

    @SerializedName("api_token")
    @Expose
    public String apiToken;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("payment_request_id")
    @Expose
    public String paymentRequestId;

    @SerializedName("payment_id")
    @Expose
    public String paymentId;

    @SerializedName("payment_status")
    @Expose
    public int paymentStatus;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("activation_status")
    @Expose
    public int activationStatus;

    @SerializedName("class_update_status")
    @Expose
    public int classUpdateStatus;

    @SerializedName("subject_update_status")
    @Expose
    public int subjectUpdateStatus;

    @SerializedName("time_slot_status")
    @Expose
    public int timeSlotStatus;

    @SerializedName("details")
    @Expose
    public UserDetails userDetails;

}
