package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TutorSubject {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("subject_name")
    @Expose
    public String name;

    @SerializedName("subject_id")
    @Expose
    public int subjectId;

    @SerializedName("stream")
    @Expose
    public String stream;

}
