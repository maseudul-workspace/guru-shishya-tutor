package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.WalletHistoryInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class WalletHistoryInteractorImpl extends AbstractInteractor implements WalletHistoryInteractor {
    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int tutor_id;

    public WalletHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int tutor_id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.tutor_id = tutor_id;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(WalletHistoryData walletHistoryData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistorySuccess(walletHistoryData);
            }
        });
    }

    @Override
    public void run() {
        final WalletHistoryWrapper walletHistoryWrapper = mRepository.fetchWalletHistory(authorization,tutor_id);
        if (walletHistoryWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!walletHistoryWrapper.status) {
            notifyError(walletHistoryWrapper.message);
        } else {
            postMessage(walletHistoryWrapper.walletHistoryData);
        }
    }
}
