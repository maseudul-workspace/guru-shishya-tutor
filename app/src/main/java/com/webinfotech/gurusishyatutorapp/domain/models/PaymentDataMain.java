package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentDataMain {

    @SerializedName("tutor_id")
    @Expose
    public int orderId;

    @SerializedName("payment_status")
    @Expose
    public int paymentStatus;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("payment_data")
    @Expose
    public PaymentData paymentData;

}
