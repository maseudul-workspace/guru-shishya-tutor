package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface PasswordChangeInteractor {
    interface Callback {
        void onPasswordChangedSuccess();
        void onPasswordChangedFail(String errorMsg, int loginError);
    }
}
