package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.RemoveTimeSlotInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class RemoveTimeSlotInteractorImpl extends AbstractInteractor implements RemoveTimeSlotInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int timeSlotId;

    public RemoveTimeSlotInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int timeSlotId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.timeSlotId = timeSlotId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onTimeSlotDeleteFailed(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onTimeSlotDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.removeTimeSlot(apiToken, timeSlotId);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
