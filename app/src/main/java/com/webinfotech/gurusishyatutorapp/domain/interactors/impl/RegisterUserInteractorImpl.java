package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.RegisterUserInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfoWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class RegisterUserInteractorImpl extends AbstractInteractor implements RegisterUserInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String mobile;
    String password;
    String confirmPassword;

    public RegisterUserInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String name, String mobile, String password, String confirmPassword) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.mobile = mobile;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.registerUser(name, mobile, password, confirmPassword);
        if (userInfoWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!userInfoWrapper.status) {
            if (userInfoWrapper.errorCode) {
                if (userInfoWrapper.errorMessage.email != null) {
                    notifyError(userInfoWrapper.errorMessage.email[0]);
                } else if (userInfoWrapper.errorMessage.mobile != null) {
                    notifyError(userInfoWrapper.errorMessage.mobile[0]);
                } else {
                    notifyError(userInfoWrapper.message);
                }
            } else {
                notifyError(userInfoWrapper.message);
            }
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
