package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;


import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.BookingDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class BookingDetailsInteractorImpl extends AbstractInteractor implements BookingDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int order_id;

    public BookingDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int order_id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.order_id = order_id;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBookingDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(BookingDetailsData bookingDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBookingDetailsSuccess(bookingDetailsData);
            }
        });
    }

    @Override
    public void run() {
        final BookingDetailsWrapper bookingDetailsWrapper = mRepository.fetchBookingDetails(authorization,order_id);
        if (bookingDetailsWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!bookingDetailsWrapper.status) {
            notifyError(bookingDetailsWrapper.message);
        } else {
            postMessage(bookingDetailsWrapper.bookingDetailsData);
        }
    }
}
