package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletHistoryData {

    @SerializedName("tutor_id")
    @Expose
    public int tutor_id;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("history")
    @Expose
    public WalletHistory[] walletHistories;

}
