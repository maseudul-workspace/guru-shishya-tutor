package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;


import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.BookingListInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingList;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingListWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class BookingListInteractorImpl extends AbstractInteractor implements BookingListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int page;

    public BookingListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.page = page;
    }


    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBookingListFail(errorMsg);
            }
        });
    }

    private void postMessage(BookingList[] bookingLists, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBookingListSuccess(bookingLists, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final BookingListWrapper bookingHistoryWrapper = mRepository.fetchBookingList(apiToken, page);
        if (bookingHistoryWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!bookingHistoryWrapper.status) {
            notifyError(bookingHistoryWrapper.message);
        } else {
            postMessage(bookingHistoryWrapper.bookingList, bookingHistoryWrapper.total_pages);
        }
    }
}
