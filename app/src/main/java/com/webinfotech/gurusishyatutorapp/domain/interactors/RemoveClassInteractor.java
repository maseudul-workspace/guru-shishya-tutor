package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface RemoveClassInteractor {
    interface Callback {
        void onClassRemoveSuccess();
        void onClassRemoveFail(String errorMsg, int loginError);
    }
}
