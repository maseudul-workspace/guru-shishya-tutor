package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface RemoveSubjectInteractor {
    interface Callback {
        void onSubjectRemoveSuccess();
        void onSubjectRemoveFail(String errorMsg, int loginError);
    }
}
