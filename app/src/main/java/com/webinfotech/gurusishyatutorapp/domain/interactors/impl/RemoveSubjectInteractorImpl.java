package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.RemoveSubjectInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class RemoveSubjectInteractorImpl extends AbstractInteractor implements RemoveSubjectInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int tutorSubjectId;

    public RemoveSubjectInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl appRepository, Callback mCallback, String apiToken, int tutorSubjectId) {
        super(threadExecutor, mainThread);
        this.mRepository = appRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.tutorSubjectId = tutorSubjectId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSubjectRemoveFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSubjectRemoveSuccess();
            }
        });
    }


    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.removeTutorSubject(apiToken, tutorSubjectId);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
