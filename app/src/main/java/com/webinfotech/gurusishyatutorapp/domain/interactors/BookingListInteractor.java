package com.webinfotech.gurusishyatutorapp.domain.interactors;


import com.webinfotech.gurusishyatutorapp.domain.models.BookingList;

public interface BookingListInteractor {
    interface Callback {
        void onGettingBookingListSuccess(BookingList[] bookingLists, int totalPage);
        void onGettingBookingListFail(String errorMsg);
    }
}
