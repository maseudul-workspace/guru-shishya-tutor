package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface AddJobInteractor {
    interface Callback {
        void onJobAddSuccess();
        void onJobAddFail(String errorMsg, int loginError);
    }
}
