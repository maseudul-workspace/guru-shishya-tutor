package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorMesage {

    @SerializedName("email")
    @Expose
    public String[] email;

    @SerializedName("mobile")
    @Expose
    public String[] mobile;

}
