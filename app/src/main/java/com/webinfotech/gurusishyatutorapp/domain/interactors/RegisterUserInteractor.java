package com.webinfotech.gurusishyatutorapp.domain.interactors;

import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterSuccess(UserInfo userInfo);
        void onRegisterFail(String errorMsg);
    }
}
