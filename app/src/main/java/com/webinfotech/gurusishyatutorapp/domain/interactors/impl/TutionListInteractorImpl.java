package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;


import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.TutionListInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.TutionList;
import com.webinfotech.gurusishyatutorapp.domain.models.TutionListWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class TutionListInteractorImpl extends AbstractInteractor implements TutionListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int page;

    public TutionListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.page = page;
    }


    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTutionListFail(errorMsg);
            }
        });
    }

    private void postMessage(TutionList[] tutionLists, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingTutionListSuccess(tutionLists, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final TutionListWrapper tutionListWrapper = mRepository.fetchTutionList(apiToken, page);
        if (tutionListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!tutionListWrapper.status) {
            notifyError(tutionListWrapper.message);
        } else {
            postMessage(tutionListWrapper.bookingList, tutionListWrapper.total_pages);
        }
    }
}
