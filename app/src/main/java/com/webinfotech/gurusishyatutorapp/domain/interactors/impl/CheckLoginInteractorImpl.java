package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.CheckLoginInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfoWrapper;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        UserInfoWrapper userInfoWrapper = mRepository.checkLogin(email, password);
        if (userInfoWrapper == null) {
            notifyError("Something went wrong");
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
