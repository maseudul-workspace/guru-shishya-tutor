package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletHistory {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("wallet_id")
    @Expose
    public int wallet_id;

    @SerializedName("total_amount")
    @Expose
    public String total_amount;

    @SerializedName("transaction_type")
    @Expose
    public String transaction_type;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("created_at")
    @Expose
    public String created_at;

}
