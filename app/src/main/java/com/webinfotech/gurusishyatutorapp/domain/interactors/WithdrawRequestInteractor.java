package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface WithdrawRequestInteractor {
    interface Callback {
        void onWithdrawRequestSentSuccess();
        void onWithdrawRequestSentFail(String errorMsg, int loginError);
    }
}
