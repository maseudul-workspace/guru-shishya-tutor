package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface AddTimeSlotInteractor {
    interface Callback {
        void onAddTimeSuccess();
        void onAddTimeFailed(String errorMsg, int loginError);
    }
}
