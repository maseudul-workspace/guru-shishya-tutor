package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface VerifyForgetPasswordOtpInteractor {
    interface Callback {
        void onVerifySuccess();
        void onVerifyFail(String errorMsg);
    }
}
