package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.WithdrawRequestInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class WithdrawRequestInteractorImpl extends AbstractInteractor implements WithdrawRequestInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String amount;
    int tutor_id;

    public WithdrawRequestInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, String amount, int tutor_id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.amount = amount;
        this.tutor_id = tutor_id;
    }

    private void notifyError(final String errorMsg, int login_error) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWithdrawRequestSentFail(errorMsg, login_error);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWithdrawRequestSentSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.withdrawRequest(authorization, amount, tutor_id);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection",0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message,commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
