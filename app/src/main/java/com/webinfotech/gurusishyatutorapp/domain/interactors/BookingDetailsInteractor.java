package com.webinfotech.gurusishyatutorapp.domain.interactors;

import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;

public interface BookingDetailsInteractor {
    interface Callback {
        void onGettingBookingDetailsSuccess(BookingDetailsData bookingDetailsData);
        void onGettingBookingDetailsFail(String errorMsg);
    }
}
