package com.webinfotech.gurusishyatutorapp.domain.interactors.impl;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.UpdateProfileInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.base.AbstractInteractor;
import com.webinfotech.gurusishyatutorapp.domain.models.CommonResponse;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class UpdateProfileInteractorImpl extends AbstractInteractor implements UpdateProfileInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int userId;
    String state;
    String city;
    String pin;
    double latitude;
    double longitude;
    String address;
    String qualification;
    String experience;
    String youtubeLink;
    String document;
    String certificate;
    String profileImage;

    public UpdateProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String apiToken, int userId, String state, String city, String pin, double latitude, double longitude, String address, String qualification, String experience, String youtubeLink, String document, String certificate, String profileImage) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.state = state;
        this.city = city;
        this.pin = pin;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.qualification = qualification;
        this.experience = experience;
        this.youtubeLink = youtubeLink;
        this.document = document;
        this.certificate = certificate;
        this.profileImage = profileImage;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateProfile(apiToken, userId, state, city, pin, latitude, longitude, address, qualification, experience, youtubeLink, document, certificate, profileImage);
        if (commonResponse == null) {
            notifyError("Please check your internet connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
