package com.webinfotech.gurusishyatutorapp.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("bank_name")
    @Expose
    public String bankName;

    @SerializedName("ac_no")
    @Expose
    public String accountNo;

    @SerializedName("ifsc")
    @Expose
    public String ifscCode;

    @SerializedName("branch_name")
    @Expose
    public String branchName;

}
