package com.webinfotech.gurusishyatutorapp.domain.interactors;

public interface TutionCompleteInteractor {
    interface Callback {
        void onTutionCompleteSuccess();
        void onTutionCompleteFail(String errorMsg);
    }
}
