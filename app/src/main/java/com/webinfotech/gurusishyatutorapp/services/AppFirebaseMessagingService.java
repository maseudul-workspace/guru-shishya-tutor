package com.webinfotech.gurusishyatutorapp.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.Html;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.webinfotech.gurusishyatutorapp.domain.models.NotificationModel;
import com.webinfotech.gurusishyatutorapp.presentation.ui.activities.SplashActivity;
import com.webinfotech.gurusishyatutorapp.util.NotificationUtils;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        String title = "";
        String body = "";
        try {
            title = remoteMessage.getData().get("title");
        } catch (NullPointerException e) {
            title = "EasyParcel";
        }

        try {
            body = remoteMessage.getData().get("body");
        } catch (NullPointerException e) {
            body = "From EasyParcel";
        }
        Intent resultIntent = new Intent(this, SplashActivity.class);
        NotificationModel notificationModel = new NotificationModel(title, body);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notificationModel, resultIntent);
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

}
