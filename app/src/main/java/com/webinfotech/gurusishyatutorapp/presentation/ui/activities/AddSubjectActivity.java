package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.domain.models.Class;
import com.webinfotech.gurusishyatutorapp.domain.models.Subject;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.AddSubjectPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.AddSubjectPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.ClassListDialog;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.SubjectsDialog;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

public class AddSubjectActivity extends AppCompatActivity implements ClassListDialog.Callback, SubjectsDialog.Callback, AddSubjectPresenter.View {

    @BindView(R.id.txt_view_class)
    TextView txtViewClass;
    @BindView(R.id.txt_view_subject)
    TextView txtViewSubject;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    ClassListDialog classListDialog;
    SubjectsDialog subjectsDialog;
    AddSubjectPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Class[] classes;
    Subject[] subjects;
    int classId = 0;
    int subjectId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subject);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Add Subject");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setClassListDialog();
        setSubjectsDialog();
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchJobDetails();
    }

    private void initialisePresenter() {
        mPresenter = new AddSubjectPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setClassListDialog() {
        classListDialog = new ClassListDialog(this, this, this);
        classListDialog.setUpDialog();
    }

    private void setSubjectsDialog() {
        subjectsDialog = new SubjectsDialog(this, this, this);
        subjectsDialog.setUpDialog();
    }

    @Override
    public void onClassSelected(int position) {
        txtViewClass.setText("Class " + classes[position].name);
        this.subjects = classes[position].subjects;
        subjectsDialog.setRecyclerViewSubject(this.subjects);
        txtViewSubject.setText("Select Subject");
        classId = classes[position].id;
        subjectId = 0;
    }

    @Override
    public void onSubjectSelect(int position) {
        txtViewSubject.setText(subjects[position].name);
        btnSubmit.setVisibility(View.VISIBLE);
        subjectId = subjects[position].id;
    }

    @Override
    public void loadData(Class[] classes) {
        this.classes = classes;
        classListDialog.setRecyclerViewClassList(classes);
    }

    @Override
    public void onJobAddSuccess() {
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.layout_class) void onClassClicked() {
        classListDialog.showDialog();
    }

    @OnClick(R.id.layout_subject) void onSubjectClicked() {
        subjectsDialog.showDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (classId == 0) {
            Toasty.warning(this, "Please choose a class").show();
        } else if (subjectId == 0) {
            Toasty.warning(this, "Please choose a subject").show();
        } else {
            mPresenter.addJob(classId, subjectId);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}