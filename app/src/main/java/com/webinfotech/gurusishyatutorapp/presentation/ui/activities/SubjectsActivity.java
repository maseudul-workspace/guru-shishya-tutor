package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.SubjectsPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.SubjectsPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutorClassesAdapter;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

public class SubjectsActivity extends AppCompatActivity implements SubjectsPresenter.View {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    SubjectsPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_class)
    RecyclerView recyclerViewClass;
    @BindView(R.id.layout_no_class)
    View layoutNoClass;
    @BindView(R.id.extended_fab)
    ExtendedFloatingActionButton extendedFloatingActionButton;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("My Subjects");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        extendedFloatingActionButton.shrink();
        initialisePresenter();
        setUpProgressDialog();
        setBottomNavigationView();
    }

    private void initialisePresenter() {
        mPresenter = new SubjectsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(TutorClassesAdapter adapter) {
        if (adapter.getItemCount() > 0) {
            recyclerViewClass.setVisibility(View.VISIBLE);
            layoutNoClass.setVisibility(View.GONE);
            recyclerViewClass.setLayoutManager(new LinearLayoutManager(this));
            recyclerViewClass.setAdapter(adapter);
            recyclerViewClass.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        } else {
            recyclerViewClass.setVisibility(View.GONE);
            layoutNoClass.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.extended_fab) void onExtendedButtonClicked() {
        if (extendedFloatingActionButton.isExtended()) {
            Intent intent = new Intent(this, AddSubjectActivity.class);
            startActivity(intent);
        } else {
            extendedFloatingActionButton.extend();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchSubjectList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id. nav_home:
                        goToHomeActivity();
                        break;
                    case R.id.nav_time_slot:
                        goToTimeSlotsActivity();
                        break;
                    case R.id.nav_profile:
                        goToUserProfile();
                        break;
                    case R.id.nav_booking:
                        goToBookingListActivity();
                        break;
                }
                return false;
            }
        });
    }

    private void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void goToTimeSlotsActivity() {
        Intent intent = new Intent(this, TimeSlotActivity.class);
        startActivity(intent);
    }

    private void goToUserProfile() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    private void goToBookingListActivity() {
        Intent intent = new Intent(this, BookingListActivity.class);
        startActivity(intent);
    }

}