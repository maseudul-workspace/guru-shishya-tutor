package com.webinfotech.gurusishyatutorapp.presentation.presenters;

public interface LoginPresenter {
    void checkLogin(String email, String password);
    interface View {
        void showLoader();
        void hideLoader();
        void goToMainActivity();
    }
}
