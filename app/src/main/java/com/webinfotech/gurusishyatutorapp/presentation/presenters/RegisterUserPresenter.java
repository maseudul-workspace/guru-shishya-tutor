package com.webinfotech.gurusishyatutorapp.presentation.presenters;

public interface RegisterUserPresenter {
    void registerUser(String name,
                      String mobile,
                      String password,
                      String confirmPassword);
    interface View {
        void onRegisterSuccess();
        void showLoader();
        void hideLoader();
    }
}
