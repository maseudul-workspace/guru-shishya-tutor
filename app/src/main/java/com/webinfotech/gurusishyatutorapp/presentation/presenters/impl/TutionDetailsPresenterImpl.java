package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.BookingDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.TutionAttendanceInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.TutionCompleteInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.BookingDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.TutionAttendanceInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.TutionCompleteInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.TutionDetailsPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class TutionDetailsPresenterImpl extends AbstractPresenter implements TutionDetailsPresenter, BookingDetailsInteractor.Callback, TutionAttendanceInteractor.Callback, TutionCompleteInteractor.Callback {

    Context mContext;
    View mView;
    int orderId;

    public TutionDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchTutionDetails(int orderId) {
        this.orderId = orderId;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        BookingDetailsInteractorImpl bookingDetailsInteractorImpl = new BookingDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(),this, userInfo.apiToken, orderId);
        bookingDetailsInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void tutionAttendance(int booking_details_id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        TutionAttendanceInteractorImpl tutionAttendanceInteractor = new TutionAttendanceInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, booking_details_id);
        tutionAttendanceInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void tutionComplete(int tution_id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        TutionCompleteInteractorImpl tutionCompleteInteractor = new TutionCompleteInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, tution_id);
        tutionCompleteInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingBookingDetailsSuccess(BookingDetailsData bookingDetailsData) {
        mView.hideLoader();
        mView.loadTutionDetails(bookingDetailsData);
    }

    @Override
    public void onGettingBookingDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onTutionAttendanceSuccess() {
        fetchTutionDetails(orderId);
        mView.hideLoader();
    }

    @Override
    public void onTutionAttendanceFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onTutionCompleteSuccess() {
        fetchTutionDetails(orderId);
        mView.hideLoader();

    }

    @Override
    public void onTutionCompleteFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }
}
