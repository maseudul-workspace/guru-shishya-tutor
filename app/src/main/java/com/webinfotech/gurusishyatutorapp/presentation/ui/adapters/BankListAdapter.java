package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.Bank;
import com.webinfotech.gurusishyatutorapp.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.ViewHolder> {

    public interface Callback {
        void onBankClicked(int position);
    }

    Context mContext;
    ArrayList<Bank> banks;
    Callback mCallback;

    public BankListAdapter(Context mContext, ArrayList<Bank> banks, Callback mCallback) {
        this.mContext = mContext;
        this.banks = banks;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_bank_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewBankName.setText(banks.get(position).bankName);
        switch (banks.get(position).bankName) {
            case "Allahabad Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.mockbank.com/bulletin/wp-content/uploads/2016/05/Allahabad-Bank.png");
                break;
            case "Axis Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.searchpng.com/wp-content/uploads/2019/01/Axis-Bank-PNG-Icon-1024x1024.png");
                break;
            case "Bandhan Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://img.etimg.com/thumb/width-640,height-480,imgsize-38916,resizemode-1,msid-71441689/industry/banking/finance/banking/bandhan-bank-gruh-finance-to-merge-on-october-17/untitled-design-87.jpg");
                break;
            case "Canara Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://media.9curry.com/uploads/organization/image/2116/canara-bank-logo.png");
                break;
            case "HDFC Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.hdfcbank.com/content/api/contentstream/723fb80a-2dde-42a3-9793-7ae1be57c87f/SEO/hdfc.png");
                break;
            case "ICICI Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://mk0bfsieletsonlt96u6.kinstacdn.com/wp-content/uploads/2015/10/ICICI-Bank.jpg");
                break;
            case "IDBI":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.viewholidaytrip.com/images/idbi-bank.jpg");
                break;
            case "IDBI - Corporate Banking":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.viewholidaytrip.com/images/idbi-bank.jpg");
                break;
            case "Indian Overseas Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://cdn.freelogovectors.net/wp-content/uploads/2019/11/indian-overseas-bank-logo.png");
                break;
            case "Kotak Mahindra Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.kotak.com/content/dam/Kotak/kotak-bank-logo.jpg");
                break;
            case "North East Small Finance Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.sentinelassam.com/wp-content/uploads/2018/11/ne.jpg");
                break;
            case "Punjab National Bank - Retail Banking":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://i.pinimg.com/originals/0b/25/37/0b2537b4353994e4454cad8e7be05c30.png");
                break;
            case "State Bank of India":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.pngitem.com/pimgs/m/247-2475218_state-bank-of-india-logo-hd-png-download.png");
                break;
            case "UCO Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://media.glassdoor.com/sqll/41807/uco-bank-squarelogo.png");
                break;
            case "Union Bank of India":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://mk0bfsieletsonlt96u6.kinstacdn.com/wp-content/uploads/2019/09/Union-Bank.jpg");
                break;
            case "Union Bank of India (Erstwhile Corporation Bank)":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://mk0bfsieletsonlt96u6.kinstacdn.com/wp-content/uploads/2019/09/Union-Bank.jpg");
                break;
            case "Yes Bank":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.multilinkrecharge.com/ReCharge/images/Yesbank.png");
                break;
            case "Yes Bank - Corporate Banking":
                GlideHelper.setImageView(mContext, holder.imgViewBank, "https://www.multilinkrecharge.com/ReCharge/images/Yesbank.png");
                break;
        }
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onBankClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return banks.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_bank)
        ImageView imgViewBank;
        @BindView(R.id.txt_view_bank_name)
        TextView txtViewBankName;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
