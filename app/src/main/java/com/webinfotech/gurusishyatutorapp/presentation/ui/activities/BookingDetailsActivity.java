
package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.avatarfirst.avatargenlib.AvatarConstants;
import com.avatarfirst.avatargenlib.AvatarGenerator;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.BookingDetailsPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.BookingDetailsPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.BookingDatesAdapter;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookingDetailsActivity extends AppCompatActivity implements BookingDetailsPresenter.View {

    @BindView(R.id.text_view_student_name)
    TextView textViewStudentName;
    @BindView(R.id.text_view_from_time)
    TextView textViewFromTime;
    @BindView(R.id.text_view_to_time)
    TextView textViewToTime;
    @BindView(R.id.text_view_payment_status)
    TextView txtViewPaymentStatus;
    @BindView(R.id.text_view_order_status)
    TextView txtViewOrderStatus;
    @BindView(R.id.red)
    View viewRed;
    @BindView(R.id.blue)
    View viewBlue;
    @BindView(R.id.green)
    View viewGreen;
    @BindView(R.id.student_profile_pic)
    ImageView imageViewStudentProfilePic;
    @BindView(R.id.booking_details_recycler_view)
    RecyclerView recyclerViewBookingDetails;
    @BindView(R.id.payment_status_linear_layout)
    LinearLayout linearLayoutPaymentStatus;
    @BindView(R.id.linearLayoutButton)
    LinearLayout linearLayoutButton;

    BookingDatesAdapter bookingDatesAdapter;
    BookingDetailsPresenterImpl mPresenter;
    GridLayoutManager gridLayoutManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Booking Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialsiePresenter();
        mPresenter.fetchBookingDetails(getIntent().getIntExtra("orderId",0));
    }

    private void initialsiePresenter() {
        mPresenter = new BookingDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadBookingDetails(BookingDetailsData bookingDetailsData) {
        imageViewStudentProfilePic.setImageDrawable( AvatarGenerator.Companion.avatarImage(this, 200, AvatarConstants.Companion.getCIRCLE(), bookingDetailsData.student_name));
        textViewStudentName.setText(capsName(bookingDetailsData.student_name));
        if(bookingDetailsData.bookingDates.length > 0)
        {
            textViewFromTime.setText(changeTimeFormat(bookingDetailsData.bookingDates[0].time_from));
            textViewToTime.setText(changeTimeFormat(bookingDetailsData.bookingDates[0].time_to));
        }


        if(bookingDetailsData.payment_status.equals("P")){
            txtViewPaymentStatus.setText("Paid");
        }
        else if(bookingDetailsData.payment_status.equals("F")){
            txtViewPaymentStatus.setText("Fail");
        }
        else if(bookingDetailsData.payment_status.equals("W"))
        {
            txtViewPaymentStatus.setText("Pending");
        }

        switch (bookingDetailsData.order_status)
        {
            case "R" :
                viewBlue.setVisibility(View.VISIBLE);
                viewGreen.setVisibility(View.GONE);
                viewRed.setVisibility(View.GONE);
                txtViewOrderStatus.setText("Requested");
                txtViewOrderStatus.setTextColor(Color.parseColor("#2196F3"));
                break;

            case "A" :
                viewBlue.setVisibility(View.GONE);
                viewGreen.setVisibility(View.VISIBLE);
                viewRed.setVisibility(View.GONE);
                txtViewOrderStatus.setText("Accepted");
                txtViewOrderStatus.setTextColor(Color.parseColor("#8BC34A"));
                break;

            case "TR" :
                viewBlue.setVisibility(View.GONE);
                viewGreen.setVisibility(View.GONE);
                viewRed.setVisibility(View.VISIBLE);
                txtViewOrderStatus.setText("Rejected");
                txtViewOrderStatus.setTextColor(Color.parseColor("#B71C1C"));
                break;

            case "C" :
                viewBlue.setVisibility(View.GONE);
                viewGreen.setVisibility(View.GONE);
                viewRed.setVisibility(View.VISIBLE);
                txtViewOrderStatus.setText("Cancelled");
                txtViewOrderStatus.setTextColor(Color.parseColor("#B71C1C"));
                break;
        }

        if(bookingDetailsData.order_status.equals("A") || bookingDetailsData.order_status.equals("TR") || bookingDetailsData.order_status.equals("C"))
        {
            linearLayoutButton.setVisibility(View.GONE);
        }

        if(bookingDetailsData.order_status.equals("TR") || bookingDetailsData.order_status.equals("C"))
        {
            linearLayoutPaymentStatus.setVisibility(View.GONE);
        }

        bookingDatesAdapter = new BookingDatesAdapter(this, bookingDetailsData.bookingDates);
        gridLayoutManager = new GridLayoutManager(this,4);
        recyclerViewBookingDetails.setLayoutManager(gridLayoutManager);
        recyclerViewBookingDetails.setAdapter(bookingDatesAdapter);
    }

    @OnClick(R.id.btn_accept) void onAcceptClicked()
    {
        mPresenter.acceptBooking(getIntent().getIntExtra("orderId",0));
    }

    @OnClick(R.id.btn_reject) void onRejectClicked()
    {
        mPresenter.rejectBooking(getIntent().getIntExtra("orderId",0));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }


    private StringBuilder capsName(String name)
    {
        StringBuilder res = new StringBuilder();

        String[] strArr = name.split(" ");
        for (String str : strArr) {
            char[] stringArray = str.trim().toCharArray();
            stringArray[0] = Character.toUpperCase(stringArray[0]);
            str = new String(stringArray);

            res.append(str).append(" ");
        }
        return res;
    }

    private String changeTimeFormat(String time) {
        SimpleDateFormat spf=new SimpleDateFormat("HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("hh:mm a");
        time = spf.format(newDate);
        return time;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}