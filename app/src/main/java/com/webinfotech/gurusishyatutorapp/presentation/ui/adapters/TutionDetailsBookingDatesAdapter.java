package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDates;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutionDetailsBookingDatesAdapter extends RecyclerView.Adapter<TutionDetailsBookingDatesAdapter.ViewHolder> {

    Context mContext;
    BookingDates[] bookingDates;
    BookingDetailsData bookingDetailsData;
    Callback mCallback;


    public interface Callback {
        void onAttendClicked(int booking_details_id);
    }

    public TutionDetailsBookingDatesAdapter(Context mContext, BookingDates[] bookingDates, Callback mCallback, BookingDetailsData bookingDetailsData) {
        this.mContext = mContext;
        this.bookingDetailsData = bookingDetailsData;
        this.bookingDates = bookingDates;
        this.mCallback = mCallback;
    }

    @NotNull
    @Override
    public TutionDetailsBookingDatesAdapter.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_view_tution_details_item, parent, false);
        TutionDetailsBookingDatesAdapter.ViewHolder holder = new TutionDetailsBookingDatesAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NotNull TutionDetailsBookingDatesAdapter.ViewHolder holder, int position) {

       holder.btn_attend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAttendClicked(bookingDates[position].id);
            }
        });

        if (bookingDetailsData.is_complete_tutor.equals("Y")) {
            holder.btn_attend.setVisibility(View.GONE);
            holder.textViewAttendance.setVisibility(View.GONE);
        } else {
            if(bookingDates[position].is_attend_tutor.equals("N")) {
                holder.btn_attend.setVisibility(View.VISIBLE);
                holder.textViewAttendance.setVisibility(View.GONE);
            } else if(bookingDates[position].is_attend_tutor.equals("Y")) {
                holder.textViewAttendance.setVisibility(View.VISIBLE);
                holder.btn_attend.setVisibility(View.GONE);
            }
        }

        holder.textViewMonth.setText(convertToMonth(bookingDates[position].date));
        holder.textViewDay.setText(convertToDay(bookingDates[position].date));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return bookingDates.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.month)
        TextView textViewMonth;
        @BindView(R.id.day)
        TextView textViewDay;
        @BindView(R.id.attendance)
        TextView textViewAttendance;
        @BindView(R.id.btn_attend)
        Button btn_attend;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(BookingDates[] bookingDates) {
        this.bookingDates = bookingDates;
        notifyDataSetChanged();
    }

    private String convertToMonth(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("MMM");
        date = spf.format(newDate);
        return date;
    }

    private String convertToDay(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-mm-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("dd");
        date = spf.format(newDate);
        return date;
    }

}
