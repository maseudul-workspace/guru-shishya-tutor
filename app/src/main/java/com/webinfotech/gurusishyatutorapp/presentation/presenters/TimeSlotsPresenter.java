package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TimeSlotsAdapter;

public interface TimeSlotsPresenter {
    void fetchTimeSlots();
    interface View {
        void loadAdapter(TimeSlotsAdapter adapter);
        void showLoader();
        void hideLoader();
    }
}
