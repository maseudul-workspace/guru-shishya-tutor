package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;


import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.AcceptBookingInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.BookingDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.RejectBookingInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.AcceptBookingInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.BookingDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.RejectBookingInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.BookingDetailsPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class BookingDetailsPresenterImpl extends AbstractPresenter implements BookingDetailsPresenter, BookingDetailsInteractor.Callback, AcceptBookingInteractor.Callback, RejectBookingInteractor.Callback {

    Context mContext;
    View mView;
    int orderId;

    public BookingDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchBookingDetails(int orderId) {
        this.orderId = orderId;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        BookingDetailsInteractorImpl bookingDetailsInteractorImpl = new BookingDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(),this, userInfo.apiToken, orderId);
        bookingDetailsInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void acceptBooking(int orderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        AcceptBookingInteractorImpl acceptBookingInteractorImpl = new AcceptBookingInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, orderId);
        acceptBookingInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void rejectBooking(int orderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        RejectBookingInteractorImpl rejectBookingInteractorImpl = new RejectBookingInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, orderId);
        rejectBookingInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingBookingDetailsSuccess(BookingDetailsData bookingDetailsData) {
        mView.hideLoader();
        mView.loadBookingDetails(bookingDetailsData);
    }

    @Override
    public void onGettingBookingDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onAcceptBookingSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Request accepted successfully", Toasty.LENGTH_SHORT).show();
        fetchBookingDetails(orderId);
    }

    @Override
    public void onAcceptBookingFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onRejectBookingSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Request rejected successfully", Toasty.LENGTH_SHORT).show();
        fetchBookingDetails(orderId);
    }

    @Override
    public void onRejectBookingFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }
}
