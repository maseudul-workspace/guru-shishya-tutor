package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.PasswordChangePresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.PasswordChangePresenterImpl;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class PasswordChangeActivity extends AppCompatActivity implements PasswordChangePresenter.View {

    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPass;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPass;
    @BindView(R.id.edit_text_current_password)
    EditText editTextCurrentPass;

    @BindView(R.id.text_input_new_password_layout)
    TextInputLayout textInputNewPassLayout;
    @BindView(R.id.text_input_confirm_password_layout)
    TextInputLayout textInputConfirmPassLayout;
    @BindView(R.id.text_input_current_password_layout)
    TextInputLayout textInputCurrentPassLayout;

    PasswordChangePresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialisePresenter();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_update_password) void onUpdatePassClicked() {
        textInputCurrentPassLayout.setError("");
        textInputNewPassLayout.setError("");
        textInputConfirmPassLayout.setError("");

        if (editTextCurrentPass.getText().toString().trim().isEmpty()) {
            textInputCurrentPassLayout.setError("Please insert your password");
        } else if(editTextCurrentPass.getText().toString().length() < 8) {
            textInputCurrentPassLayout.setError("password must be at least 8 characters");
        } else if (editTextNewPass.getText().toString().trim().isEmpty()) {
            textInputNewPassLayout.setError("Please insert your password");
        } else if(editTextNewPass.getText().toString().length() < 8) {
            textInputNewPassLayout.setError("password must be at least 8 characters");
        } else if (editTextConfirmPass.getText().toString().trim().isEmpty()) {
            textInputConfirmPassLayout.setError("Please insert your password");
        } else if (!editTextNewPass.getText().toString().trim().equals(editTextConfirmPass.getText().toString().trim())) {
            textInputConfirmPassLayout.setError("Password and confirm password does not match");
        }
        else
        {
            mPresenter.changePassword( editTextCurrentPass.getText().toString(),editTextConfirmPass.getText().toString(),editTextNewPass.getText().toString());
        }
    }


    private void initialisePresenter() {
        mPresenter = new PasswordChangePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void onChangePasswordSuccess() {
        finish();
        Toasty.success(this, "Password updated successfully", Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLoginError() {

    }
}