package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchJobDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.RemoveClassInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.RemoveSubjectInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.FetchJobDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.RemoveClassInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.RemoveSubjectInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.JobDetails;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.SubjectsPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutorClassesAdapter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SubjectsPresenterImpl extends AbstractPresenter implements SubjectsPresenter,
                                                                        FetchJobDetailsInteractor.Callback,
                                                                        TutorClassesAdapter.Callback,
                                                                        RemoveClassInteractor.Callback,
                                                                        RemoveSubjectInteractor.Callback
{

    Context mContext;
    SubjectsPresenter.View mView;

    public SubjectsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubjectList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchJobDetailsInteractorImpl fetchJobDetailsInteractor = new FetchJobDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchJobDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingJobDetailsSuccess(JobDetails jobDetails) {
        TutorClassesAdapter adapter = new TutorClassesAdapter(mContext, jobDetails.tutorClasses, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingJobDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onClassDeleteClicked(int classId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            RemoveClassInteractorImpl removeClassInteractor = new RemoveClassInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, classId);
            removeClassInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onSubjectDeleteClicked(int subjectId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            RemoveSubjectInteractorImpl removeSubjectInteractor = new RemoveSubjectInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, subjectId);
            removeSubjectInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onSubjectRemoveSuccess() {
        fetchSubjectList();
    }

    @Override
    public void onSubjectRemoveFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onClassRemoveSuccess() {
        fetchSubjectList();
    }

    @Override
    public void onClassRemoveFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
