package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.SendForgetPasswordOtpInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.VerifyForgetPasswordOtpInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.SendForgetPasswordOtpInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.VerifyForgetPasswordInteractorImpl;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.ForgetPasswordOtpVerifyPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;


public class ForgetPasswordOtpVerifyPresenterImpl extends AbstractPresenter implements ForgetPasswordOtpVerifyPresenter, SendForgetPasswordOtpInteractor.Callback, VerifyForgetPasswordOtpInteractor.Callback {

    Context mContext;
    ForgetPasswordOtpVerifyPresenter.View mView;

    public ForgetPasswordOtpVerifyPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phone) {
        SendForgetPasswordOtpInteractorImpl sendOtpForgetPasswordInteractor = new SendForgetPasswordOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        sendOtpForgetPasswordInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void verifyOtp(String phone, String otp) {
        VerifyForgetPasswordInteractorImpl verifyForgetPasswordInteractor = new VerifyForgetPasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone, otp);
        verifyForgetPasswordInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onOtpSendSuccess() {
        mView.hideLoader();
        mView.onOtpSendSuccess();
    }

    @Override
    public void onOtpSendFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onVerifySuccess() {
        mView.hideLoader();
        mView.onOtpVerifySuccess();
    }

    @Override
    public void onVerifyFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
