package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutionListRecyclerViewAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutorClassesAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TimeSlotsAdapter;

public interface MainPresenter {
    void fetchUserProfile();
    void fetchJobDetails();
    void fetchWalletHistory(int tutor_id);
    void fetchTutionList(int page, String refresh);
    void withdrawRequest(String amount);
    void sendAppId(String app_id);
    interface View {
        void loadUserDetails(UserInfo userInfo);
        void loadWalletHistory(WalletHistoryData walletHistoryData);
        void loadClasses(TutorClassesAdapter adapter, TimeSlotsAdapter timeSlotsAdapter);
        void loadAdapter(TutionListRecyclerViewAdapter adapter, int totalPage);
        void goToTutionListDetails(int orderId);
        void hidePaginationLoader();
        void onWithdrawRequestSuccess();
        void showLoader();
        void hideLoader();
    }
}
