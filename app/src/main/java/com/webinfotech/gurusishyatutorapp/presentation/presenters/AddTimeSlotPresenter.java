package com.webinfotech.gurusishyatutorapp.presentation.presenters;

public interface AddTimeSlotPresenter {
    void addTimeSlot(String fromTime, String toTime);
    interface View {
        void onTimeSlotAddSuccess();
        void showLoader();
        void hideLoader();
    }
}
