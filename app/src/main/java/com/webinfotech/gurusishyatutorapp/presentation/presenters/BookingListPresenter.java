package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.BookingListRecyclerViewAdapter;

public interface BookingListPresenter {
    void fetchBookingHistory(int page, String refresh);
    interface View {
        void loadAdapter(BookingListRecyclerViewAdapter adapter, int totalPage);
        void goToBookingListDetails(int orderId);
        void showLoader();
        void hideLoader();
        void hidePaginationLoader();
    }
}
