package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutorClassesAdapter;

public interface SubjectsPresenter {
    void fetchSubjectList();
    interface View {
        void loadAdapter(TutorClassesAdapter adapter);
        void showLoader();
        void hideLoader();
    }
}
