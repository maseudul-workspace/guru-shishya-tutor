package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.PaymentMethodsCallback;
import com.razorpay.PaymentResultWithDataListener;
import com.razorpay.Razorpay;
import com.razorpay.ValidationListener;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.domain.models.Bank;
import com.webinfotech.gurusishyatutorapp.domain.models.ExpiryDate;
import com.webinfotech.gurusishyatutorapp.domain.models.PaymentData;
import com.webinfotech.gurusishyatutorapp.domain.models.PaymentDataMain;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletList;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.PaymentPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.PaymentPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.BankListAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.CardExpiryMonthAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.CardExpiryYearAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.WalletListAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.BankListDialog;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.CardExpiryMonthDialog;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.CardExpiryYearDialog;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class PaymentActivity extends AppCompatActivity  implements CardExpiryMonthAdapter.Callback, CardExpiryYearAdapter.Callback, BankListAdapter.Callback, WalletListAdapter.Callback, PaymentPresenter.View, PaymentResultWithDataListener {

    @BindView(R.id.payment_webview)
    WebView webView;
    @BindView(R.id.frame)
    FrameLayout frameLayout;
    @BindView(R.id.layout_payment_options)
    View layoutPaymentOptions;
    TextView txtViewExpiryMonth;
    TextView txtViewExpiryYear;
    TextView txtViewBankName;
    EditText editTextCard;
    EditText editTextCardName;
    EditText editTextCvv;
    ArrayList<ExpiryDate> expiryYears = new ArrayList<>();
    ArrayList<ExpiryDate> expiryMonths = new ArrayList<>();
    ArrayList<WalletList> walletLists = new ArrayList<>();
    CardExpiryYearDialog cardExpiryYearDialog;
    CardExpiryMonthDialog cardExpiryMonthDialog;
    BankListDialog bankListDialog;
    private JSONObject payload;
    private Razorpay razorpay;
    ArrayList<Bank> banks = new ArrayList<>();
    WalletListAdapter walletListAdapter;
    int paymentMethod = 0;
    String expiryMonth;
    String expiryYear;
    String bankCode;
    int upiPaymentMethod;
    EditText editTextUpiAddress;
    String walletCode;
    @BindView(R.id.txt_view_payable_amount)
    TextView txtViewPayableAmount;
    PaymentPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    PaymentData paymentData;
    private int orderStatus;
    private String errorMsg;
    private String paymentId;
    private String amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Payment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        setCardExpiryMonthDialog();
        setCardExpiryYearDialog();
        setBankListDialog();
        initRazorPay();
        initialisePresenter();
        mPresenter.placeOrder();
    }

    private void initialisePresenter() {
        mPresenter = new PaymentPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }
    private void initRazorPay() {
        razorpay = new Razorpay(this);
        razorpay.changeApiKey("rzp_live_Ge2fk7zqLs2kLo");
//        razorpay.changeApiKey("rzp_test_Z7IuNz4aVP4NRq");

        razorpay.getPaymentMethods(new PaymentMethodsCallback() {
            @Override
            public void onPaymentMethodsReceived(String s) {
                inflateLists(s);
            }

            @Override
            public void onError(String s) {

            }
        });

        razorpay.setWebView(webView);

    }

    private void inflateLists(String result) {
        try {
            JSONObject paymentMethods = new JSONObject(result);
            JSONObject banksListJSON = paymentMethods.getJSONObject("netbanking");
            JSONObject walletListJSON = paymentMethods.getJSONObject("wallet");



            Iterator<String> itr1 = banksListJSON.keys();
            while (itr1.hasNext()) {
                String key = itr1.next();
                try {
                    Bank bank = new Bank(key, banksListJSON.getString(key));
                    banks.add(bank);
                } catch (JSONException e) {
                    Log.d("Reading Banks List", "" + e.getMessage());
                }
            }

            Iterator<String> itr2 = walletListJSON.keys();
            while (itr2.hasNext()) {
                String key = itr2.next();
                try {
                    if (walletListJSON.getBoolean(key)) {
                        WalletList walletList = new WalletList(key, false);
                        walletLists.add(walletList);
                    }
                } catch (JSONException e) {
                    Log.d("Reading Wallets List", "" + e.getMessage());
                }
            }

            BankListAdapter adapter = new BankListAdapter(this, banks, this);
            bankListDialog.setRecyclerViewBankList(adapter);

            walletListAdapter = new WalletListAdapter(this, walletLists, this);

        } catch (Exception e) {
        }
    }

    @OnClick(R.id.layout_card_payment) void onCardPaymentClicked() {

        expiryMonth = "";
        expiryYear = "";

        paymentMethod = 1;

        frameLayout.removeAllViews();
        LayoutInflater.from(PaymentActivity.this).inflate(R.layout.fragment_option_card_payment, frameLayout, true);
        layoutPaymentOptions.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);

        TextView txtViewCardPaymentGoBack = (TextView) findViewById(R.id.txt_view_card_payment_go_back);
        txtViewExpiryMonth = (TextView) findViewById(R.id.txt_view_expiry_month);
        txtViewExpiryYear = (TextView) findViewById(R.id.txt_view_expiry_year);
        View expiryMonthLayout = (View) findViewById(R.id.view_expiry_month);
        View expiryYearLayout = (View) findViewById(R.id.view_expiry_year);
        editTextCard = (EditText) findViewById(R.id.edit_text_card_number);
        editTextCardName = (EditText) findViewById(R.id.edit_text_card_name);
        editTextCvv = (EditText) findViewById(R.id.edit_text_cvv);


        txtViewCardPaymentGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPaymentOptions.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
            }
        });
        editTextCard.addTextChangedListener(new TextWatcher() {

            boolean isDelete;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(before==0)
                    isDelete=false;
                else
                    isDelete=true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                String source = s.toString();
                int length=source.length();

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(source);

                if(length>0 && length%5==0)
                {
                    if(isDelete)
                        stringBuilder.deleteCharAt(length-1);
                    else
                        stringBuilder.insert(length-1," ");

                    editTextCard.setText(stringBuilder);
                    editTextCard.setSelection(editTextCard.getText().length());

                }
            }
        });

        expiryMonthLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardExpiryMonthDialog.showDialog();
            }
        });

        expiryYearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardExpiryYearDialog.showDialog();
            }
        });

    }

    @OnClick(R.id.layout_net_banking) void onNetBankingClicked() {
        paymentMethod = 3;
        bankCode = "";
        frameLayout.removeAllViews();
        LayoutInflater.from(PaymentActivity.this).inflate(R.layout.fragment_option_net_banking, frameLayout, true);
        layoutPaymentOptions.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);

        View layoutBankSelect = (View) findViewById(R.id.layout_banks_select);
        TextView txtViewNetBankingBack = (TextView) findViewById(R.id.txt_view_net_baking_go_back);
        txtViewNetBankingBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPaymentOptions.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
            }
        });

        layoutBankSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bankListDialog.showDialog();
            }
        });

        txtViewBankName = (TextView) findViewById(R.id.txt_view_bank_name);

    }

    @OnClick(R.id.layout_google_pay) void onUpiPaymentClicked() {
        paymentMethod = 2;
        upiPaymentMethod = 0;
        frameLayout.removeAllViews();
        LayoutInflater.from(PaymentActivity.this).inflate(R.layout.fragment_option_upi_payment, frameLayout, true);
        layoutPaymentOptions.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);

        TextView txtViewUpiGoBack = (TextView) findViewById(R.id.txt_view_upi_go_back);
        txtViewUpiGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPaymentOptions.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
            }
        });

        RadioButton radioButtonGooglePay = (RadioButton) findViewById(R.id.radio_btn_google_pay);
        RadioButton radioButtonOtherUpi = (RadioButton) findViewById(R.id.radio_btn_other_upi);

        editTextUpiAddress = (EditText) findViewById(R.id.edit_text_upi_address);

        radioButtonGooglePay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextUpiAddress.setVisibility(View.GONE);
                radioButtonGooglePay.setChecked(true);
                radioButtonOtherUpi.setChecked(false);
                upiPaymentMethod = 1;
            }
        });

        radioButtonOtherUpi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upiPaymentMethod = 2;
                editTextUpiAddress.setVisibility(View.VISIBLE);
                radioButtonGooglePay.setChecked(false);
                radioButtonOtherUpi.setChecked(true);
            }
        });

        View layoutRadioBtnGooglePay = (View) findViewById(R.id.layout_google_pay_radio_btn);
        View layoutRadioBtnUpi = (View) findViewById(R.id.layout_upi_radio_btn);
        layoutRadioBtnGooglePay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upiPaymentMethod = 1;
                editTextUpiAddress.setVisibility(View.GONE);
                radioButtonGooglePay.setChecked(true);
                radioButtonOtherUpi.setChecked(false);
            }
        });
        layoutRadioBtnUpi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upiPaymentMethod = 2;
                editTextUpiAddress.setVisibility(View.VISIBLE);
                radioButtonGooglePay.setChecked(false);
                radioButtonOtherUpi.setChecked(true);
            }
        });
    }

    @OnClick(R.id.layout_wallet) void onWalletClicked() {
        paymentMethod = 4;
        walletCode = "";
        frameLayout.removeAllViews();
        LayoutInflater.from(PaymentActivity.this).inflate(R.layout.fragment_option_wallet_payment, frameLayout, true);
        layoutPaymentOptions.setVisibility(View.GONE);
        frameLayout.setVisibility(View.VISIBLE);

        TextView txtViewWalletBack = (TextView) findViewById(R.id.txt_view_wallet_go_back);
        txtViewWalletBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPaymentOptions.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view_wallet_list);
        recyclerView.setAdapter(walletListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onExpiryMonthClicked(int position) {
        cardExpiryMonthDialog.hideDialog();
        txtViewExpiryMonth.setText(expiryMonths.get(position).appearedDate);
        expiryMonth = expiryMonths.get(position).exactDate;
    }

    @Override
    public void onExpiryYearClicked(int position) {
        cardExpiryYearDialog.hideDialog();
        txtViewExpiryYear.setText(expiryYears.get(position).appearedDate);
        expiryYear = expiryYears.get(position).exactDate;
    }

    private void setCardExpiryYearDialog() {

        cardExpiryYearDialog = new CardExpiryYearDialog(this, this);
        cardExpiryYearDialog.setUpDialog();
        ExpiryDate expiryDate1 = new ExpiryDate(1, "2021", "21");
        ExpiryDate expiryDate2 = new ExpiryDate(2, "2022", "22");
        ExpiryDate expiryDate3 = new ExpiryDate(3, "2023", "23");
        ExpiryDate expiryDate4 = new ExpiryDate(4, "2024", "24");
        ExpiryDate expiryDate5 = new ExpiryDate(5, "2025", "25");
        ExpiryDate expiryDate6 = new ExpiryDate(6, "2026", "26");
        ExpiryDate expiryDate7 = new ExpiryDate(7, "2027", "27");
        ExpiryDate expiryDate8 = new ExpiryDate(8, "2028", "28");
        ExpiryDate expiryDate9= new ExpiryDate(9, "2029", "29");
        ExpiryDate expiryDate10 = new ExpiryDate(10, "2030", "30");

        expiryYears.add(expiryDate1);
        expiryYears.add(expiryDate2);
        expiryYears.add(expiryDate3);
        expiryYears.add(expiryDate4);
        expiryYears.add(expiryDate5);
        expiryYears.add(expiryDate6);
        expiryYears.add(expiryDate7);
        expiryYears.add(expiryDate8);
        expiryYears.add(expiryDate9);
        expiryYears.add(expiryDate10);

        CardExpiryYearAdapter adapter = new CardExpiryYearAdapter(this, expiryYears, this);
        cardExpiryYearDialog.setExpiryYearAdapter(adapter);

    }

    private void setCardExpiryMonthDialog() {

        cardExpiryMonthDialog = new CardExpiryMonthDialog(this, this);
        cardExpiryMonthDialog.setUpDialog();
        ExpiryDate expiryDate1 = new ExpiryDate(1, "01 (JAN)", "01");
        ExpiryDate expiryDate2 = new ExpiryDate(2, "02 (FEB)", "02");
        ExpiryDate expiryDate3 = new ExpiryDate(3, "03 (MAR)", "03");
        ExpiryDate expiryDate4 = new ExpiryDate(4, "04 (APR)", "04");
        ExpiryDate expiryDate5 = new ExpiryDate(5, "05 (MAY)", "05");
        ExpiryDate expiryDate6 = new ExpiryDate(6, "06 (JUN)", "06");
        ExpiryDate expiryDate7 = new ExpiryDate(7, "07 (JUL)", "07");
        ExpiryDate expiryDate8 = new ExpiryDate(8, "08 (AUG)", "08");
        ExpiryDate expiryDate9 = new ExpiryDate(9, "09 (SEPT)", "09");
        ExpiryDate expiryDate10 = new ExpiryDate(10, "10 (OCT)", "10");
        ExpiryDate expiryDate11 = new ExpiryDate(11, "11 (NOV)", "11");
        ExpiryDate expiryDate12 = new ExpiryDate(12, "12 (DEC)", "12");


        expiryMonths.add(expiryDate1);
        expiryMonths.add(expiryDate2);
        expiryMonths.add(expiryDate3);
        expiryMonths.add(expiryDate4);
        expiryMonths.add(expiryDate5);
        expiryMonths.add(expiryDate6);
        expiryMonths.add(expiryDate7);
        expiryMonths.add(expiryDate8);
        expiryMonths.add(expiryDate9);
        expiryMonths.add(expiryDate10);
        expiryMonths.add(expiryDate11);
        expiryMonths.add(expiryDate12);

        CardExpiryMonthAdapter adapter = new CardExpiryMonthAdapter(this, expiryMonths, this);
        cardExpiryMonthDialog.setExpiryMonthAdapter(adapter);

    }

    private void setBankListDialog() {
        bankListDialog = new BankListDialog(this, this);
        bankListDialog.setUpDialog();
    }

    @Override
    public void onBankClicked(int position) {
        bankListDialog.hideDialog();
        txtViewBankName.setText(banks.get(position).bankName);
        bankCode = banks.get(position).bankCode;
    }

    @Override
    public void onWalletSelected(int position) {
        for (int i = 0; i < walletLists.size(); i++) {
            if (i == position) {
                walletLists.get(i).isSelected = true;
            } else {
                walletLists.get(i).isSelected = false;
            }
        }
        walletListAdapter.setData(walletLists);
        walletCode = walletLists.get(position).walletCode;
    }

    @Override
    public void loadData(PaymentDataMain paymentDataMain) {
        amount = paymentDataMain.amount;
        txtViewPayableAmount.setText("Rs. " + paymentDataMain.amount);
        paymentData = paymentDataMain.paymentData;
    }

    @Override
    public void onPaymentVerificationSuccess() {
        orderStatus = 1;
        goToPaymentResponseActivity();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_place_order) void onPlaceOrderClicked() {

        if (paymentMethod == 0) {
            Toasty.warning(this, "Please Select a Payment Method").show();
        } else {
            switch (paymentMethod) {
                case 1:
                    if (checkValidCardInput()) {
                        initiatePayment(paymentData);
                    }
                    break;
                case 2:
                    if (upiPaymentMethod == 1) {
                        initiatePayment(paymentData);
                    } else if (upiPaymentMethod == 2) {
                        if (editTextUpiAddress.getText().toString().trim().isEmpty()) {
                            Toasty.warning(this, "Please Enter UPI Address").show();
                        } else {
                            initiatePayment(paymentData);
                        }
                    } else {
                        Toasty.warning(this, "Please Select a Payment Method").show();
                    }
                    break;
                case 3:
                    if (!bankCode.isEmpty()) {
                        initiatePayment(paymentData);
                    } else {
                        Toasty.warning(this, "Please Select a Bank").show();
                    }
                    break;
                case 4:
                    if (!walletCode.isEmpty()) {
                        initiatePayment(paymentData);
                    } else {
                        Toasty.warning(this, "Please Select a Wallet").show();
                    }
                    break;
            }
        }
    }

    private boolean checkValidCardInput() {
        if (    editTextCard.getText().toString().trim().isEmpty() ||
                editTextCardName.getText().toString().trim().isEmpty() ||
                editTextCvv.getText().toString().trim().isEmpty() ||
                expiryYear.isEmpty() ||
                expiryMonth.isEmpty()
        ) {
            Toasty.warning(this, "All Fields Are Mandatory").show();
            return false;
        } else if (editTextCard.getText().toString().trim().length() < 16) {
            Toasty.warning(this, "Please Insert a Valid Card No").show();
            return false;
        } else if (editTextCvv.toString().trim().length() < 3) {
            Toasty.warning(this, "Please Insert a Valid CVV No").show();
            return false;
        } else {
            return true;
        }
    }

    public void initiatePayment(PaymentData paymentData) {
        switch (paymentMethod) {
            case 1:
                if (checkValidCardInput()) {
                    try {
                        payload = new JSONObject("{currency: 'INR'}");
                        payload.put("contact", paymentData.mobile);
                        payload.put("order_id", paymentData.order_id);
                        payload.put("amount", paymentData.amount);
                        payload.put("email", "razthemoster@gmail.com");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        payload.put("method", "card");
                        payload.put("card[name]", editTextCardName.getText().toString());
                        payload.put("card[number]", editTextCard.getText().toString().replace(" ", ""));
                        payload.put("card[expiry_month]", expiryMonth);
                        payload.put("card[expiry_year]", expiryYear);
                        payload.put("card[cvv]", editTextCvv.getText().toString());
                        sendRequest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case 2:
                if (upiPaymentMethod == 1) {
                    try {
                        payload = new JSONObject("{currency: 'INR'}");
                        payload.put("order_id", paymentData.order_id);
                        payload.put("amount", paymentData.amount);
                        payload.put("contact", paymentData.mobile);
                        payload.put("email", "razthemoster@gmail.com");
                        //payload.put("upi_app_package_name", "com.google.android.apps.nbu.paisa.user");
                        payload.put("display_logo", true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONArray jArray = new JSONArray();
                        jArray.put("in.org.npci.upiapp");
                        jArray.put("com.snapwork.hdfc");
                        payload.put("description","Credits towards Guru Shishya");
                        //payload.put("key_id","rzp_test_kEVtCVFWAjUQPG");
                        payload.put("method", "upi");
                        payload.put("_[flow]", "intent");
                        payload.put("preferred_apps_order", jArray);
                        payload.put("other_apps_order", jArray);
                        sendRequest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (upiPaymentMethod == 2) {
                    if (editTextUpiAddress.getText().toString().trim().isEmpty()) {
                        Toasty.warning(this, "Please Enter UPI Address").show();
                    } else {
                        try {
                            payload = new JSONObject("{currency: 'INR'}");
                            payload.put("order_id", paymentData.order_id);
                            payload.put("amount", paymentData.amount);
                            payload.put("contact", paymentData.mobile);
                            payload.put("email", "razthemoster@gmail.com");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            payload.put("method", "upi");
                            payload.put("vpa", editTextUpiAddress.getText().toString());
                            sendRequest();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toasty.warning(this, "Please Select a Payment Method").show();
                }
                break;
            case 3:
                if (!bankCode.isEmpty()) {
                    try {
                        payload = new JSONObject("{currency: 'INR'}");
                        payload.put("order_id", paymentData.order_id);
                        payload.put("amount", paymentData.amount);
                        payload.put("contact", paymentData.mobile);
                        payload.put("email", "razthemoster@gmail.com");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        payload.put("method", "netbanking");
                        payload.put("bank", bankCode);
                        sendRequest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toasty.warning(this, "Please Select a Bank").show();
                }
                break;
            case 4:
                if (!walletCode.isEmpty()) {
                    try {
                        payload = new JSONObject("{currency: 'INR'}");
                        payload.put("order_id", paymentData.order_id);
                        payload.put("amount", paymentData.amount);
                        payload.put("contact", paymentData.mobile);
                        payload.put("email", "razthemoster@gmail.com");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        payload.put("method", "wallet");
                        payload.put("wallet", walletCode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sendRequest();
                } else {
                    Toasty.warning(this, "Please Select a Wallet").show();
                }
                break;
        }
    }

    private void sendRequest() {
        razorpay.validateFields(payload, new ValidationListener() {
            @Override
            public void onValidationSuccess() {
                try {
                    webView.setVisibility(View.VISIBLE);
                    razorpay.submit(payload, new PaymentResultWithDataListener() {
                        @Override
                        public void onPaymentSuccess(String s, com.razorpay.PaymentData paymentData) {
                            paymentId = paymentData.getPaymentId();
                            mPresenter.verifyPayment(paymentData.getOrderId(), paymentData.getPaymentId(), paymentData.getSignature());
                        }

                        @Override
                        public void onPaymentError(int i, String s, com.razorpay.PaymentData paymentData) {
                            orderStatus = 2;
                            webView.setVisibility(View.GONE);
                            errorMsg = s;
                            goToPaymentResponseActivity();
                        }

                    });
                } catch (Exception e) {
                }
            }

            @Override
            public void onValidationError(Map<String, String> map) {
                Toast.makeText(PaymentActivity.this, "Validation: " + map.get("field") + " " + map.get("description"), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void goToPaymentResponseActivity() {
        Intent intent = new Intent(this, PaymentResponseActivity.class);
        intent.putExtra("orderId", paymentData.order_id);
        intent.putExtra("orderStatus", orderStatus);
        intent.putExtra("paymentId", paymentId);
        intent.putExtra("amount", amount);
        intent.putExtra("errorMsg", errorMsg);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* callback for permission requested from android */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (razorpay != null) {
            razorpay.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        razorpay.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onPaymentSuccess(String s, com.razorpay.PaymentData paymentData) {
        paymentId = paymentData.getPaymentId();
        mPresenter.verifyPayment(paymentData.getOrderId(), paymentData.getPaymentId(), paymentData.getSignature());
    }

    @Override
    public void onPaymentError(int i, String s, com.razorpay.PaymentData paymentData) {
        orderStatus = 2;
        webView.setVisibility(View.GONE);
        errorMsg = s;
        goToPaymentResponseActivity();
    }
}