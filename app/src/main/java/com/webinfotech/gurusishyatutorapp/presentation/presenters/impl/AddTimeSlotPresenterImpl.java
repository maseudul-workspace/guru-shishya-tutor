package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.AddTimeSlotInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.AddTimeSlotInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.AddTimeSlotPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddTimeSlotPresenterImpl extends AbstractPresenter implements  AddTimeSlotPresenter,
                                                                            AddTimeSlotInteractor.Callback {

    Context mContext;
    AddTimeSlotPresenter.View mView;

    public AddTimeSlotPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addTimeSlot(String fromTime, String toTime) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            AddTimeSlotInteractorImpl addTimeSlotInteractor = new AddTimeSlotInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, fromTime, toTime);
            addTimeSlotInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddTimeSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Time Slot Added").show();
        mView.onTimeSlotAddSuccess();
    }

    @Override
    public void onAddTimeFailed(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg).show();
        mView.hideLoader();
    }
}
