package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.SuccessDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PaymentResponseActivity extends AppCompatActivity implements SuccessDialog.Callback
{

    @BindView(R.id.layout_success_payment)
    View layoutSuccess;
    @BindView(R.id.layout_failed_payment)
    View layoutFailed;
    @BindView(R.id.txt_view_order_id)
    TextView txtViewOrderId;
    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_transaction_id)
    TextView txtViewPaymentId;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_name)
    TextView txtViewName;
    String orderId;
    int orderStatus;
    String amount;
    String paymentId;
    private SuccessDialog successDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_response);
        ButterKnife.bind(this);
        orderId = getIntent().getStringExtra("orderId");
        paymentId = getIntent().getStringExtra("paymentId");
        orderStatus = getIntent().getIntExtra("orderStatus", 0);
        amount = getIntent().getStringExtra("amount");
        setData();
        setSuccessDialog();
        successDialog.showDialog();
    }

    private void setData() {
        txtViewOrderId.setText(orderId);
        txtViewOrderDate.setText(getDate());
        txtViewAmount.setText("Rs. " + amount);
        txtViewPaymentId.setText(paymentId);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        txtViewName.setText(userInfo.name);
        if (orderStatus == 1) {
            layoutSuccess.setVisibility(View.VISIBLE);
        } else {
            layoutFailed.setVisibility(View.VISIBLE);
        }
    }

    private void setSuccessDialog() {
        successDialog = new SuccessDialog(this, this, this);
        successDialog.setUpDialog();
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MMMM dd, yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onConfirmClicked() {
        successDialog.hideDialog();
    }
}