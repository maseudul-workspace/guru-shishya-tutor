package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.domain.models.UserDetails;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.UserProfilePresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.UserProfilePresenterImpl;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;
import com.webinfotech.gurusishyatutorapp.util.GlideHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.webinfotech.gurusishyatutorapp.util.Helper.calculateFileSize;
import static com.webinfotech.gurusishyatutorapp.util.Helper.getRealPathFromURI;
import static com.webinfotech.gurusishyatutorapp.util.Helper.saveImage;

public class UserProfileActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks,
                                                                        GoogleApiClient.OnConnectionFailedListener, UserProfilePresenter.View {

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    String[] appPremisions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    double latitude = 0;
    double longitude = 0;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_qualification)
    EditText editTextQualification;
    @BindView(R.id.edit_text_experience)
    EditText editTextExperience;
    @BindView(R.id.edit_text_youtube_link)
    EditText editTextYoutubeLink;
    @BindView(R.id.img_view_document)
    ImageView imgViewDocument;
    @BindView(R.id.img_view_education_proof)
    ImageView imgViewEducationProof;
    @BindView(R.id.img_view_profile_image)
    ImageView imgViewProfileImage;
    String[] filePremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int FILE_PERMISSIONS_REQUEST_CODE = 1245;
    private static int REQUEST_PIC;
    String document = null;
    String educationProof = null;
    String profileImage = null;
    UserProfilePresenterImpl mPresenter;
    ProgressDialog progressDialog;
    boolean isImageProvided = false;
    boolean isEducationProof = false;
    boolean isProfileImage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("My Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        setBottomNavigationView();
        mPresenter.fetchUserProfile();
    }

    private void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private boolean checkAndRequestFilePermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : filePremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), FILE_PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_update_profile) void onProfileUpdateClicked() {
        if (    editTextState.getText().toString().trim().isEmpty() ||
                editTextCity.getText().toString().trim().isEmpty() ||
                editTextPin.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Please fill the required fields").show();
        } else if (document == null && !isImageProvided) {
            Toasty.warning(this, "Please upload an identity proof").show();
        } else if (educationProof == null && !isEducationProof) {
            Toasty.warning(this, "Please upload an education proof").show();
        } else if (profileImage == null && !isProfileImage) {
            Toasty.warning(this, "Please upload profile image").show();
        } else {
            mPresenter.updateProfile(   editTextState.getText().toString(),
                                        editTextCity.getText().toString(),
                                        editTextPin.getText().toString(),
                                        latitude,
                                        longitude,
                                        editTextAddress.getText().toString(),
                                        editTextQualification.getText().toString(),
                                        editTextExperience.getText().toString(),
                                        editTextYoutubeLink.getText().toString(),
                                        document,
                                        educationProof,
                                        profileImage
                    );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                buildGoogleApiClient();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs location permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
        else if (requestCode == FILE_PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                loadImageChooser();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestFilePermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    private AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        List<Address> addresses = null;
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String postalCode = addresses.get(0).getPostalCode();
        editTextCity.setText(city);
        editTextState.setText(state);
        editTextPin.setText(postalCode);
        editTextAddress.setText(address);

    }

    @Override
    public void loadUserDetails(UserDetails userDetails) {
        if (userDetails == null) {
            if (checkAndRequestPermissions()) {
                buildGoogleApiClient();
            }
        } else {
            editTextPin.setText(userDetails.pin);
            editTextCity.setText(userDetails.city);
            editTextState.setText(userDetails.state);
            editTextAddress.setText(userDetails.address);
            editTextExperience.setText(userDetails.experience);
            editTextQualification.setText(userDetails.qualification);
            editTextYoutubeLink.setText(userDetails.youtubeLink);
            longitude = userDetails.longitude;
            latitude = userDetails.latitude;
            if (userDetails.document != null) {
                isImageProvided = true;
                GlideHelper.setImageViewCustomRoundedCorners(this, imgViewDocument, getResources().getString(R.string.base_url) + "images/tutor/" + userDetails.document, 150);
            }
            if (userDetails.certificate != null) {
                isEducationProof = true;
                GlideHelper.setImageViewCustomRoundedCorners(this, imgViewEducationProof, getResources().getString(R.string.base_url) + "images/tutor/" + userDetails.certificate, 150);
            }
            if (userDetails.profileImage != null) {
                isProfileImage = true;
                GlideHelper.setImageViewCustomRoundedCorners(this, imgViewProfileImage, getResources().getString(R.string.base_url) + "images/tutor/" + userDetails.profileImage, 150);
            }
        }
    }

    @Override
    public void onUpdateProfileSuccess() {
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.layout_document) void onDocumentClicked() {
        REQUEST_PIC = 1000;
        if (checkAndRequestFilePermissions()) {
            loadImageChooser();
        }
    }

    @OnClick(R.id.layout_education_proof) void onEducationProofClicked() {
        REQUEST_PIC = 1001;
        if (checkAndRequestFilePermissions()) {
            loadImageChooser();
        }
    }

    @OnClick(R.id.layout_profile_image) void onProfileImageClicked() {
        REQUEST_PIC = 1002;
        if (checkAndRequestFilePermissions()) {
            loadImageChooser();
        }
    }

    private void loadImageChooser() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_PIC);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1000:
                    if (data.getData() != null) {
                        document = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(document) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imgViewDocument, data.getData(), 150);
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        document = saveImage(photo);
                        if (calculateFileSize(document) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewDocument, photo, 150);
                        }
                    }
                    break;
                case 1001:
                    if (data.getData() != null) {
                        educationProof = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(educationProof) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imgViewEducationProof, data.getData(), 150);
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        educationProof = saveImage(photo);
                        if (calculateFileSize(educationProof) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewEducationProof, photo, 150);
                        }
                    }
                    break;
                case 1002:
                    if (data.getData() != null) {
                        profileImage = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(profileImage) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imgViewProfileImage, data.getData(), 150);
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        profileImage = saveImage(photo);
                        if (calculateFileSize(profileImage) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewProfileImage, photo, 150);
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id. nav_home:
                        goToHomeActivity();
                        break;
                    case R.id.nav_time_slot:
                        goToTimeSlotsActivity();
                        break;
                    case R.id.nav_subject:
                        goToSubjectListActivity();
                        break;
                    case R.id.nav_booking:
                        goToBookingListActivity();
                        break;
                }
                return false;
            }
        });
    }


    private void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void goToSubjectListActivity() {
        Intent intent = new Intent(this, SubjectsActivity.class);
        startActivity(intent);
    }

    private void goToTimeSlotsActivity() {
        Intent intent = new Intent(this, TimeSlotActivity.class);
        startActivity(intent);
    }

    private void goToBookingListActivity() {
        Intent intent = new Intent(this, BookingListActivity.class);
        startActivity(intent);
    }

}