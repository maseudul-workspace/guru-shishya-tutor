package com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import com.webinfotech.gurusishyatutorapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class WithdrawDialog {

    @BindView(R.id.edit_text_amount)
    EditText editTextAmount;

    Context mContext;
    Callback mCallback;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;

    public interface Callback {
        void onWithdrawRequestClicked(String amount);
    }



    public WithdrawDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_withdraw_request, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.btn_request_withdraw) void onWithdrawRequestClicked()
    {
        if(editTextAmount.getText().toString().isEmpty())
        {
            Toasty.warning(mContext, "Please enter amount" , Toasty.LENGTH_SHORT).show();
        }
        else
        {
            mCallback.onWithdrawRequestClicked(editTextAmount.getText().toString());
        }
    }
}
