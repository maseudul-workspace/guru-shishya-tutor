package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.avatarfirst.avatargenlib.AvatarConstants;
import com.avatarfirst.avatargenlib.AvatarGenerator;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.TutionList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutionListRecyclerViewAdapter extends RecyclerView.Adapter<TutionListRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    TutionList[] tutionLists;
    Callback mCallback;


    public interface Callback {
        void onTutionClicked(int OrderId);
    }


    public TutionListRecyclerViewAdapter(Context mContext, TutionList[] tutionLists, Callback mCallback) {
        this.mContext = mContext;
        this.tutionLists = tutionLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public TutionListRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
           View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_tution_list_item, parent, false);
           TutionListRecyclerViewAdapter.ViewHolder holder = new TutionListRecyclerViewAdapter.ViewHolder(view);
           return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TutionListRecyclerViewAdapter.ViewHolder holder, int position) {

        holder.txtViewTutorName.setText(capsName(tutionLists[position].student_name));
        holder.txtViewClass.setText("Class "+tutionLists[position].class_name);
        holder.txtViewDate.setText(convertDate(tutionLists[position].booking_date));
        holder.imageViewStudentProfilePic.setImageDrawable( AvatarGenerator.Companion.avatarImage(mContext, 200, AvatarConstants.Companion.getRECTANGLE(), tutionLists[position].student_name));
        holder.mainLayoutClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onTutionClicked(tutionLists[position].order_id);
            }
        });

        if(tutionLists[position].payment_status.equals("P")){
            holder.txtViewPaymentStatus.setText("Paid");
        }

        if ("A".equals(tutionLists[position].order_status)) {
            holder.viewBlue.setVisibility(View.GONE);
            holder.viewGreen.setVisibility(View.VISIBLE);
            holder.viewRed.setVisibility(View.GONE);
            holder.txtViewOrderStatus.setText("Accepted");
            holder.txtViewOrderStatus.setTextColor(Color.parseColor("#8BC34A"));
        }
    }

    @Override
    public int getItemCount() {
        return tutionLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_Name)
        TextView txtViewTutorName;
        @BindView(R.id.txt_view_Class)
        TextView txtViewClass;
        @BindView(R.id.txt_view_Date)
        TextView txtViewDate;
        @BindView(R.id.text_view_payment_status)
        TextView txtViewPaymentStatus;
        @BindView(R.id.text_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.student_profile_pic)
        ImageView imageViewStudentProfilePic;
        @BindView(R.id.red)
        View viewRed;
        @BindView(R.id.blue)
        View viewBlue;
        @BindView(R.id.green)
        View viewGreen;
        @BindView(R.id.main_layout_class)
        CardView mainLayoutClass;
        @BindView(R.id.payment_status_linear_layout)
        LinearLayout linearLayoutPaymentStatus;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void updateData(TutionList[] tutionLists) {
        this.tutionLists = tutionLists;
        notifyDataSetChanged();
    }


    private String convertDate(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEE, MMMM d");
        date = spf.format(newDate);
        return date;
    }


    private StringBuilder capsName(String name)
    {
        StringBuilder res = new StringBuilder();

        String[] strArr = name.split(" ");
        for (String str : strArr) {
            char[] stringArray = str.trim().toCharArray();
            stringArray[0] = Character.toUpperCase(stringArray[0]);
            str = new String(stringArray);

            res.append(str).append(" ");
        }
        return res;
    }
}
