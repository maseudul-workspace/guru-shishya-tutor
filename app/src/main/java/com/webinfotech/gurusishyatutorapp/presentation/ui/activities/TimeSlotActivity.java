package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.TimeSlotsPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.TimeSlotsPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TimeSlotsAdapter;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

public class TimeSlotActivity extends AppCompatActivity implements TimeSlotsPresenter.View {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    TimeSlotsPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_time_slots)
    RecyclerView recyclerViewTimeSlots;
    @BindView(R.id.layout_no_time_slots)
    View layoutNoTimeSlots;
    @BindView(R.id.extended_fab)
    ExtendedFloatingActionButton extendedFloatingActionButton;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot);
        getSupportActionBar().setTitle("My Time Slots");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        extendedFloatingActionButton.shrink();
        initialisePresenter();
        setUpProgressDialog();
        setBottomNavigationView();
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new TimeSlotsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(TimeSlotsAdapter adapter) {
        if (adapter.getItemCount() > 0) {
            recyclerViewTimeSlots.setVisibility(View.VISIBLE);
            layoutNoTimeSlots.setVisibility(View.GONE);
            recyclerViewTimeSlots.setAdapter(adapter);
            recyclerViewTimeSlots.setLayoutManager(new LinearLayoutManager(this));
        } else {
            recyclerViewTimeSlots.setVisibility(View.GONE);
            layoutNoTimeSlots.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.extended_fab) void onExtendedButtonClicked() {
        if (extendedFloatingActionButton.isExtended()) {
            Intent intent = new Intent(this, AddTimeSlotActivity.class);
            startActivity(intent);
        } else {
            extendedFloatingActionButton.extend();
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchTimeSlots();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id. nav_home:
                        goToHomeActivity();
                        break;
                    case R.id.nav_subject:
                        goToSubjectListActivity();
                        break;
                    case R.id.nav_profile:
                        goToUserProfile();
                        break;
                    case R.id.nav_booking:
                        goToBookingListActivity();
                        break;
                }
                return false;
            }
        });
    }


    private void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void goToSubjectListActivity() {
        Intent intent = new Intent(this, SubjectsActivity.class);
        startActivity(intent);
    }

    private void goToBookingListActivity() {
        Intent intent = new Intent(this, BookingListActivity.class);
        startActivity(intent);
    }

    private void goToUserProfile() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

}