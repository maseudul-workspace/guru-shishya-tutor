package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingDates;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingDatesAdapter extends RecyclerView.Adapter<BookingDatesAdapter.ViewHolder> {

    Context mContext;
    BookingDates[] bookingDates;

    public BookingDatesAdapter(Context mContext, BookingDates[] bookingDates) {
        this.mContext = mContext;
        this.bookingDates = bookingDates;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_view_booking_details_item, parent, false);
       BookingDatesAdapter.ViewHolder holder = new BookingDatesAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
       holder.textViewMonth.setText(convertToMonth(bookingDates[position].date));
       holder.textViewDay.setText(convertToDay(bookingDates[position].date));
    }

    @Override
    public int getItemCount() {
        return bookingDates.length;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.month)
        TextView textViewMonth;
        @BindView(R.id.day)
        TextView textViewDay;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String convertToMonth(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("MMM");
        date = spf.format(newDate);
        return date;
    }

    private String convertToDay(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-mm-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("dd");
        date = spf.format(newDate);
        return date;
    }

}
