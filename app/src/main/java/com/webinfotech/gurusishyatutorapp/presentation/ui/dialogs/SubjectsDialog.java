package com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.Subject;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.SubjectsAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectsDialog implements SubjectsAdapter.Callback {

    @Override
    public void onSubjectSelect(int position) {
        hideDialog();
        mCallback.onSubjectSelect(position);
    }

    public interface Callback {
        void onSubjectSelect(int position);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.recycler_view_subject)
    RecyclerView recyclerViewSubject;
    @BindView(R.id.no_subject_layout)
    View noSubjectLayout;

    public SubjectsDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.dialog_subject_list, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setRecyclerViewSubject(Subject[] subjects) {
        if (subjects.length == 0) {
            recyclerViewSubject.setVisibility(View.GONE);
            noSubjectLayout.setVisibility(View.VISIBLE);
        } else {
            SubjectsAdapter adapter = new SubjectsAdapter(mContext, subjects, this);
            recyclerViewSubject.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerViewSubject.setAdapter(adapter);
            recyclerViewSubject.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
            recyclerViewSubject.setVisibility(View.VISIBLE);
            noSubjectLayout.setVisibility(View.GONE);
        }
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
