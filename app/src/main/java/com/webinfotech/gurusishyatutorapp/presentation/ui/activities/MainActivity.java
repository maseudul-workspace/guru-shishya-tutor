package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.MainPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutionListRecyclerViewAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutorClassesAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TimeSlotsAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.WithdrawDialog;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;
import com.webinfotech.gurusishyatutorapp.util.GlideHelper;

public class MainActivity extends AppCompatActivity implements MainPresenter.View, WithdrawDialog.Callback {


    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.txt_view_activation_status)
    TextView txtViewActivationStatus;
    @BindView(R.id.text_view_balance)
    TextView txtViewBalance;
    @BindView(R.id.btn_activate)
    Button btnActivate;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_view_user)
    TextView txtViewUser;
    @BindView(R.id.txt_view_update_status)
    TextView txtViewUpdateStatus;
    @BindView(R.id.imageView_user_dp)
    ImageView imageViewUserDp;
    @BindView(R.id.recycler_view_tution_list)
    RecyclerView recyclerViewTutionList;
    @BindView(R.id.pagination_progressbar)
    View paginationProgressbar;
    MainPresenterImpl mPresenter;
    WithdrawDialog withdrawDialog;
    ProgressDialog progressDialog;
    LinearLayoutManager layoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    int orderId;
    protected int tutorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Html.fromHtml("<b>GuruShishya Tutor</b>"));
        setBottomNavigationView();
        setWithdrawDialog();
        setUpProgressDialog();
        initialisePresenter();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mPresenter.sendAppId(refreshedToken);
        refresh();
        AndroidApplication androidApplication = (AndroidApplication) this.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        this.tutorId = userInfo.userId;
        mPresenter.fetchWalletHistory(userInfo.userId);
        orderId = getIntent().getIntExtra("orderId", 0);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setWithdrawDialog() {
        withdrawDialog = new WithdrawDialog(this, this, this);
        withdrawDialog.setUpDialog();
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_subject:
                        goToSubjectListActivity();
                        break;
                    case R.id.nav_time_slot:
                        goToTimeSlotsActivity();
                        break;
                    case R.id.nav_profile:
                        goToUserProfile();
                        break;
                    case R.id.nav_booking:
                        goToBookingListActivity();
                        break;
                }
                return false;
            }
        });
    }

    private void goToSubjectListActivity() {
        Intent intent = new Intent(this, SubjectsActivity.class);
        startActivity(intent);
    }

    private void goToTimeSlotsActivity() {
        Intent intent = new Intent(this, TimeSlotActivity.class);
        startActivity(intent);
    }

    private void goToUserProfile() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    private void goToBookingListActivity() {
        Intent intent = new Intent(this, BookingListActivity.class);
        startActivity(intent);
    }

    @Override
    public void loadUserDetails(UserInfo userInfo) {
        txtViewUser.setText(userInfo.name);
        if (userInfo.userDetails == null) {
            btnActivate.setVisibility(View.GONE);
            txtViewUpdateStatus.setVisibility(View.VISIBLE);
        } else {
            if (userInfo.activationStatus == 2) {
                txtViewActivationStatus.setText("Active");
                txtViewActivationStatus.setTextColor(getResources().getColor(R.color.md_green_500));
                btnActivate.setVisibility(View.GONE);
                txtViewUpdateStatus.setVisibility(View.GONE);
            } else {
                btnActivate.setVisibility(View.VISIBLE);
                txtViewUpdateStatus.setVisibility(View.GONE);
            }
        }

        try {
            GlideHelper.setImageViewCustomRoundedCorners(this, imageViewUserDp, getResources().getString(R.string.base_url) + "images/tutor/" + userInfo.userDetails.profileImage, 200); }
        catch (Exception e)
        {
            System.out.println(e);
        }

    }

    @Override
    public void loadClasses(TutorClassesAdapter adapter, TimeSlotsAdapter timeSlotsAdapter) {
    }

    @Override
    public void loadAdapter(TutionListRecyclerViewAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewTutionList.setLayoutManager(layoutManager);
        recyclerViewTutionList.setAdapter(adapter);
        recyclerViewTutionList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchTutionList(pageNo,"");
                    }
                }
            }
        });

    }

    @Override
    public void goToTutionListDetails(int orderId) {
        Intent intent = new Intent(this, TutionDetailsActivity.class);
        intent.putExtra("orderId", orderId);
        startActivity(intent);
    }

    @Override
    public void hidePaginationLoader() {
        paginationProgressbar.setVisibility(View.GONE);
    }

    @Override
    public void onWithdrawRequestSuccess() {
        withdrawDialog.hideDialog();
        Toasty.warning(this, "Withdraw Request Sent Successfully", Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void loadWalletHistory(WalletHistoryData walletHistoryData) {
        txtViewBalance.setText("₹ "+walletHistoryData.amount);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @OnClick(R.id.btn_activate) void onBtnActivateClicked() {
        Intent intent = new Intent(this, PaymentActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.img_view_logout) void onLogoutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.img_view_change_password) void onChangeClicked() {
        Intent intent = new Intent(this, PasswordChangeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.imageView_user_dp) void onImageClicked()
    {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_withdraw) void onWithdrawClicked() {
        withdrawDialog.showDialog();
    }

    @OnClick(R.id.btn_wallet_history) void onWalletHistoryClicked() {
        Intent intent = new Intent(this, WalletHistoryActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pageNo = 1;
        totalPage = 1;
        isScrolling = false;
        mPresenter.fetchTutionList(pageNo, "refresh");
        mPresenter.fetchJobDetails();
        mPresenter.fetchUserProfile();
        mPresenter.fetchWalletHistory(tutorId);
    }

    @Override
    public void onWithdrawRequestClicked(String amount) {
        mPresenter.withdrawRequest(amount);
        mPresenter.fetchWalletHistory(tutorId);
    }

    private void refresh()
    {
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            onResume();
            swipeRefreshLayout.setRefreshing(false);
        });
    }
}