package com.webinfotech.gurusishyatutorapp.presentation.presenters;

public interface PasswordChangePresenter {
    void changePassword(String current_password, String confirm_password, String new_password);
    interface View {
        void onChangePasswordSuccess();
        void showLoader();
        void hideLoader();
        void onLoginError();
    }
}
