package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.RegisterUserPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.RegisterUserPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs.SuccessDialog;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

public class RegistrationActivity extends AppCompatActivity implements RegisterUserPresenter.View, SuccessDialog.Callback {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPassword;
    RegisterUserPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    SuccessDialog successDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_registration);
        ButterKnife.bind(this);
        editTextPhone.setText(getIntent().getStringExtra("phone"));
        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        setSuccessDialog();
    }

    private void initialisePresenter() {
        mPresenter = new RegisterUserPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setSuccessDialog() {
        successDialog = new SuccessDialog(this, this, this);
        successDialog.setUpDialog();
    }

    @Override
    public void onRegisterSuccess() {
        onConfirmClicked();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextName.getText().toString().trim().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty() ||
                editTextConfirmPassword.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Please fill all the fields").show();
        } else if(editTextPassword.getText().toString().length() < 8) {
            Toasty.warning(this, "Password must be minumn 8 charcters in length").show();
        } else if(editTextPhone.getText().toString().length() != 10) {
            Toasty.warning(this, "Mobile must be 10 characters").show();
        } else if (!editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) {
            Toasty.warning(this, "Password mismatch").show();
        } else {
            mPresenter.registerUser(
                    editTextName.getText().toString(),
                    editTextPhone.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextConfirmPassword.getText().toString()
            );
            showLoader();
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfirmClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}