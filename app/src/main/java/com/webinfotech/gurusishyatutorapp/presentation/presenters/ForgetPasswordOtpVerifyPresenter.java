package com.webinfotech.gurusishyatutorapp.presentation.presenters;

public interface ForgetPasswordOtpVerifyPresenter {
    void sendOtp(String phone);
    void verifyOtp(String phone, String otp);
    interface View {
        void onOtpSendSuccess();
        void onOtpVerifySuccess();
        void showLoader();
        void hideLoader();
    }
}
