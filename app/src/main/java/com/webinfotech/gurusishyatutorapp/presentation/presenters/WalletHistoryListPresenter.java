package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;

public interface WalletHistoryListPresenter {
    void fetchWalletHistory(int tutor_id);
    interface View {
        void loadWalletHistory(WalletHistoryData walletHistoryData);
        void showLoader();
        void hideLoader();
    }
}
