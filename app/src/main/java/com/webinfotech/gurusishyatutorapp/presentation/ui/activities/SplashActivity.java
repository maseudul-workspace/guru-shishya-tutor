package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import es.dmoral.toasty.Toasty;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isNetworkConnected()) {
                    if (checkLoginInfo()) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    finish();
                } else {
                    Toasty.warning(getApplicationContext(), "No internet connection", Toast.LENGTH_LONG, true).show();
                }
            }
        }, 5000);
    }

    private boolean checkLoginInfo() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userDetails = androidApplication.getUserInfo(this);
        if (userDetails != null) {
//            Log.e("LogMsg", "Api Token: " + userDetails.apiToken);
//            Log.e("LogMsg", "User Id: " + userDetails.userId);
            return true;
        }
        return false;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

}