package com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.CardExpiryYearAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CardExpiryYearDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.recycler_view_expiry_dates)
    RecyclerView recyclerViewExpiryDates;

    public CardExpiryYearDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.dialog_expiry_date, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setExpiryYearAdapter(CardExpiryYearAdapter adapter) {
        recyclerViewExpiryDates.setAdapter(adapter);
        recyclerViewExpiryDates.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewExpiryDates.addItemDecoration(new DividerItemDecoration(recyclerViewExpiryDates.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
