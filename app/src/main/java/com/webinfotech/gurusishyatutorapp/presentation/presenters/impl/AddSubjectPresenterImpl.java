package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.AddJobInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchJobDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.AddJobInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.FetchJobDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.JobDetails;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.AddSubjectPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddSubjectPresenterImpl extends AbstractPresenter implements   AddSubjectPresenter,
                                                                            FetchJobDetailsInteractor.Callback,
                                                                            AddJobInteractor.Callback
{

    Context mContext;
    AddSubjectPresenter.View mView;

    public AddSubjectPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchJobDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchJobDetailsInteractorImpl fetchJobDetailsInteractor = new FetchJobDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchJobDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void addJob(int classId, int subjectId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            AddJobInteractorImpl addJobInteractor = new AddJobInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, userInfo.userId, classId, subjectId);
            addJobInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingJobDetailsSuccess(JobDetails jobDetails) {
        mView.hideLoader();
        mView.loadData(jobDetails.classes);
    }

    @Override
    public void onGettingJobDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onJobAddSuccess() {
        mView.hideLoader();
        mView.onJobAddSuccess();
        Toasty.success(mContext, "Subject Added").show();
    }

    @Override
    public void onJobAddFail(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg).show();
        mView.hideLoader();
    }
}
