package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;


import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.BookingListInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.BookingListInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingList;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.BookingListPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.BookingListRecyclerViewAdapter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;


public class BookingListPresenterImpl extends AbstractPresenter implements BookingListPresenter, BookingListInteractor.Callback, BookingListRecyclerViewAdapter.Callback{

    Context mContext;
    BookingListPresenter.View mView;
    BookingList[] newBookingLists = null;
    BookingListRecyclerViewAdapter adapter;


    public BookingListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onGettingBookingListSuccess(BookingList[] bookingLists, int totalPage) {

        BookingList[] tempBookingHistories;
        tempBookingHistories = newBookingLists;
        try {
            int len1 = tempBookingHistories.length;
            int len2 = bookingLists.length;
            newBookingLists = new BookingList[len1 + len2];
            System.arraycopy(tempBookingHistories, 0, newBookingLists, 0, len1);
            System.arraycopy(bookingLists, 0, newBookingLists, len1, len2);
            adapter.updateData(newBookingLists);
        }catch (NullPointerException e){
            newBookingLists = bookingLists;
            adapter = new BookingListRecyclerViewAdapter(mContext, bookingLists, this);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
        mView.hidePaginationLoader();
    }

    @Override
    public void onGettingBookingListFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void fetchBookingHistory(int page, String refresh) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (refresh.equals("refresh")) {
            newBookingLists = null;
        }
        if (userInfo != null) {
            BookingListInteractorImpl bookingListInteractorImpl = new BookingListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, page);
            bookingListInteractorImpl.execute();
        }
        mView.showLoader();
    }

    @Override
    public void onOrderClicked(int OrderId) {
            mView.goToBookingListDetails(OrderId);
    }

}
