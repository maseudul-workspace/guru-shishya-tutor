package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;

public interface TutionDetailsPresenter {
    void fetchTutionDetails(int orderId);
    void tutionAttendance(int booking_details_id);
    void tutionComplete(int tution_id);
    interface View{
        void loadTutionDetails(BookingDetailsData bookingDetailsData);
        void showLoader();
        void hideLoader();
    }
}
