package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.avatarfirst.avatargenlib.AvatarConstants;
import com.avatarfirst.avatargenlib.AvatarGenerator;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.BookingList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingListRecyclerViewAdapter extends RecyclerView.Adapter<BookingListRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    BookingList[] bookingLists;
    Callback mCallback;


    public interface Callback {
        void onOrderClicked(int OrderId);
    }


    public BookingListRecyclerViewAdapter(Context mContext, BookingList[] bookingLists, Callback mCallback) {
        this.mContext = mContext;
        this.bookingLists = bookingLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public BookingListRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
           View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_booking_list_item, parent, false);
           BookingListRecyclerViewAdapter.ViewHolder holder = new BookingListRecyclerViewAdapter.ViewHolder(view);
           return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BookingListRecyclerViewAdapter.ViewHolder holder, int position) {

        holder.txtViewTutorName.setText(capsName(bookingLists[position].student_name));
        holder.txtViewClass.setText("Class "+bookingLists[position].class_name);
        holder.txtViewDate.setText(convertDate(bookingLists[position].booking_date));
        holder.imageViewStudentProfilePic.setImageDrawable( AvatarGenerator.Companion.avatarImage(mContext, 200, AvatarConstants.Companion.getRECTANGLE(), bookingLists[position].student_name));
        holder.mainLayoutClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onOrderClicked(bookingLists[position].order_id);
            }
        });

        if(bookingLists[position].payment_status.equals("P")){
            holder.txtViewPaymentStatus.setText("Paid");
        }
        else if(bookingLists[position].payment_status.equals("F")){
            holder.txtViewPaymentStatus.setText("Fail");
        }
        else if(bookingLists[position].payment_status.equals("W"))
        {
            holder.txtViewPaymentStatus.setText("Pending");
        }

        switch (bookingLists[position].order_status)
        {
            case "R" :
                holder.viewBlue.setVisibility(View.VISIBLE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Requested");
                holder.txtViewOrderStatus.setTextColor(Color.parseColor("#2196F3"));
                break;

            case "A" :
                 holder.viewBlue.setVisibility(View.GONE);
                 holder.viewGreen.setVisibility(View.VISIBLE);
                 holder.viewRed.setVisibility(View.GONE);
                 holder.txtViewOrderStatus.setText("Accepted");
                holder.txtViewOrderStatus.setTextColor(Color.parseColor("#8BC34A"));
                break;

            case "TR" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setText("Rejected");
                holder.txtViewOrderStatus.setTextColor(Color.parseColor("#B71C1C"));
                break;

            case "C" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setText("Cancelled");
                holder.txtViewOrderStatus.setTextColor(Color.parseColor("#B71C1C"));
                break;
        }

        if(bookingLists[position].order_status.equals("TR") || bookingLists[position].order_status.equals("C"))
        {
            holder.linearLayoutPaymentStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return bookingLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_Name)
        TextView txtViewTutorName;
        @BindView(R.id.txt_view_Class)
        TextView txtViewClass;
        @BindView(R.id.txt_view_Date)
        TextView txtViewDate;
        @BindView(R.id.text_view_payment_status)
        TextView txtViewPaymentStatus;
        @BindView(R.id.text_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.student_profile_pic)
        ImageView imageViewStudentProfilePic;
        @BindView(R.id.red)
        View viewRed;
        @BindView(R.id.blue)
        View viewBlue;
        @BindView(R.id.green)
        View viewGreen;
        @BindView(R.id.main_layout_class)
        CardView mainLayoutClass;
        @BindView(R.id.payment_status_linear_layout)
        LinearLayout linearLayoutPaymentStatus;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void updateData(BookingList[] bookingLists) {
        this.bookingLists = bookingLists;
        notifyDataSetChanged();
    }


    private String convertDate(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEE, MMMM d");
        date = spf.format(newDate);
        return date;
    }


    private StringBuilder capsName(String name)
    {
        StringBuilder res = new StringBuilder();

        String[] strArr = name.split(" ");
        for (String str : strArr) {
            char[] stringArray = str.trim().toCharArray();
            stringArray[0] = Character.toUpperCase(stringArray[0]);
            str = new String(stringArray);

            res.append(str).append(" ");
        }
        return res;
    }
}
