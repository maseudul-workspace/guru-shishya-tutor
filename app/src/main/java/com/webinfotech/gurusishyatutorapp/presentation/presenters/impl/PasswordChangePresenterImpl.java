package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;
import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.PasswordChangeInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.PasswordChangeInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.PasswordChangePresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class PasswordChangePresenterImpl extends AbstractPresenter implements PasswordChangePresenter, PasswordChangeInteractor.Callback {

    Context mContext;
    PasswordChangePresenter.View mView;

    public PasswordChangePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onPasswordChangedSuccess() {
        mView.hideLoader();
        mView.onChangePasswordSuccess();
    }

    @Override
    public void onPasswordChangedFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if(loginError == 1)
        {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext,null);
            mView.onLoginError();
            Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
        }
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void changePassword( String current_password, String new_password, String confirm_password) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        PasswordChangeInteractorImpl passwordChangedInteractorImpl = new PasswordChangeInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(),this, userInfo.apiToken, userInfo.userId, current_password, new_password, confirm_password );
        passwordChangedInteractorImpl.execute();
        mView.showLoader();
    }
}
