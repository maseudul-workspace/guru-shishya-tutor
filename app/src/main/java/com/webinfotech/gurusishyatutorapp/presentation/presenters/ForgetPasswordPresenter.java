package com.webinfotech.gurusishyatutorapp.presentation.presenters;

public interface ForgetPasswordPresenter {
    void requestPasswordChange(String otp, String mobile, String password);
    interface View {
        void onPasswordRequestSuccess();
        void showLoader();
        void hideLoader();
    }
}
