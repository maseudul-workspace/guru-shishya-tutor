package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchJobDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.RemoveTimeSlotInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.FetchJobDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.RemoveTimeSlotInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.JobDetails;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.TimeSlotsPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TimeSlotsAdapter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class TimeSlotsPresenterImpl extends AbstractPresenter implements    TimeSlotsPresenter,
                                                                            FetchJobDetailsInteractor.Callback,
                                                                            TimeSlotsAdapter.Callback,
                                                                            RemoveTimeSlotInteractor.Callback {

    Context mContext;
    TimeSlotsPresenter.View mView;

    public TimeSlotsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchTimeSlots() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchJobDetailsInteractorImpl fetchJobDetailsInteractor = new FetchJobDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchJobDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingJobDetailsSuccess(JobDetails jobDetails) {
        TimeSlotsAdapter adapter = new TimeSlotsAdapter(mContext, jobDetails.timeSlots, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingJobDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onDeleteTimeSlotClicked(int timeSlotId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            RemoveTimeSlotInteractorImpl removeTimeSlotInteractor = new RemoveTimeSlotInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, timeSlotId);
            removeTimeSlotInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onTimeSlotDeleteSuccess() {
        fetchTimeSlots();
    }

    @Override
    public void onTimeSlotDeleteFailed(String errorMsg, int loginWE) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
