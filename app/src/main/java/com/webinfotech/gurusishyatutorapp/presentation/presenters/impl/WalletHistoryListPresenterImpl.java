package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.WalletHistoryInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.WalletHistoryInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.WalletHistoryListPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WalletHistoryListPresenterImpl extends AbstractPresenter implements WalletHistoryListPresenter, WalletHistoryInteractor.Callback {

    Context mContext;
    View mView;

    public WalletHistoryListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onGettingWalletHistorySuccess(WalletHistoryData walletHistoryData) {
        mView.hideLoader();
        mView.loadWalletHistory(walletHistoryData);
    }

    @Override
    public void onGettingWalletHistoryFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void fetchWalletHistory(int tutor_id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        WalletHistoryInteractorImpl walletHistoryInteractor = new WalletHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, tutor_id);
        walletHistoryInteractor.execute();
        mView.showLoader();
    }
}
