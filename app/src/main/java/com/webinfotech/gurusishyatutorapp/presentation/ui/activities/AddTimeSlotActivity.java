package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.TimePicker;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.AddTimeSlotPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.AddTimeSlotPresenterImpl;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddTimeSlotActivity extends AppCompatActivity implements AddTimeSlotPresenter.View {

    @BindView(R.id.txt_view_from_time)
    TextView txtViewFromTime;
    @BindView(R.id.txt_view_to_time)
    TextView txtViewToTime;
    String fromTime = null;
    String toTime = null;
    ProgressDialog progressDialog;
    AddTimeSlotPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_time_slot);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Add Time Slot");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new AddTimeSlotPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.layout_time_from) void onTimeFromClicked() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        fromTime = new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
                        txtViewFromTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @OnClick(R.id.layout_time_to) void onTimeToClicked() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        toTime = new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
                        txtViewToTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void onTimeSlotAddSuccess() {
        finish();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (fromTime == null) {
            Toasty.warning(this, "Please choose your class starting time").show();
        } else if (toTime == null) {
            Toasty.warning(this, "Please choose your class ending time").show();
        } else {
            mPresenter.addTimeSlot(fromTime, toTime);
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}