package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.domain.models.UserDetails;

public interface UserProfilePresenter {
    void fetchUserProfile();
    void updateProfile( String state,
                        String city,
                        String pin,
                        double latitude,
                        double longitude,
                        String address,
                        String qualification,
                        String experience,
                        String youtubeLink,
                        String document,
                        String certificate,
                        String profileImage
                        );
    interface View {
        void loadUserDetails(UserDetails userDetails);
        void onUpdateProfileSuccess();
        void showLoader();
        void hideLoader();
    }
}
