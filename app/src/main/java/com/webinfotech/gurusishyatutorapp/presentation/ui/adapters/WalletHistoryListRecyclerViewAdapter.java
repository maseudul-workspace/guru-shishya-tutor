package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHistoryListRecyclerViewAdapter extends RecyclerView.Adapter<WalletHistoryListRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    WalletHistory[] walletHistories;


    public WalletHistoryListRecyclerViewAdapter(Context mContext, WalletHistory[] walletHistories) {
        this.mContext = mContext;
        this.walletHistories = walletHistories;
    }

    @NonNull
    @Override
    public WalletHistoryListRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
           View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_wallet_history_list_item, parent, false);
           WalletHistoryListRecyclerViewAdapter.ViewHolder holder = new WalletHistoryListRecyclerViewAdapter.ViewHolder(view);
           return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull WalletHistoryListRecyclerViewAdapter.ViewHolder holder, int position) {

        holder.txtViewTime.setText(convertDateToTime(walletHistories[position].created_at));
        holder.txtViewDate.setText(convertDate(walletHistories[position].created_at));
        holder.txtViewComment.setText(walletHistories[position].comment);
        if(walletHistories[position].transaction_type.equals("1")) {
            holder.txtViewStatus.setText("Debited");
        } else if(walletHistories[position].transaction_type.equals("2")) {
            holder.txtViewStatus.setText("Credited");
        }

        holder.txtViewAmount.setText("₹ "+walletHistories[position].total_amount);
    }

    @Override
    public int getItemCount() {
        return walletHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.text_view_wallet_history_list_amount)
        TextView txtViewAmount;
        @BindView(R.id.text_view_comment)
        TextView txtViewComment;
        @BindView(R.id.text_view_date)
        TextView txtViewDate;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    private String convertDateToTime(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("h:mm a");
        date = spf.format(newDate);
        return date;
    }

    private String convertDate(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEE, d MMM");
        date = spf.format(newDate);
        return date;
    }
}
