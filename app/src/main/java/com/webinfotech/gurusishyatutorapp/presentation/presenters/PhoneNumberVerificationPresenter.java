package com.webinfotech.gurusishyatutorapp.presentation.presenters;

public interface PhoneNumberVerificationPresenter {
    void sendOtp(String phoneNo);
    void verifyOtp(String phoneNo, String otp);
    interface View {
        void showLoader();
        void hideLoader();
        void onSendOtpSuccess();
        void onOtpVerifySucces();
    }
}
