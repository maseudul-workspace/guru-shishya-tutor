package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.razorpay.PaymentResultListener;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.domain.models.PaymentDataMain;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.PaymentPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.PaymentPresenterImpl;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

public class TestPaymentActivity extends AppCompatActivity implements  PaymentPresenter.View, PaymentResultListener {

    ProgressDialog progressDialog;
    PaymentPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_payment);
        setProgressDialog();
        initialisePresenter();
        mPresenter.placeOrder();
    }

    private void initialisePresenter() {
        mPresenter = new PaymentPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(PaymentDataMain paymentDataMain) {
//        Checkout checkout = new Checkout();
//        checkout.setKeyID("rzp_live_Ge2fk7zqLs2kLo");
//        /**
//         * Set your logo here
//         */
//        checkout.setImage(R.drawable.app_icon);
//
//        /**
//         * Reference to current activity
//         */
//        /**
//         * Pass your payment options to the Razorpay Checkout as a JSONObject
//         */
//        try {
//            JSONObject options = new JSONObject();
//
//            options.put("name", "Merchant Name");
//            options.put("description", "Reference No. #123456");
//            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
//            options.put("order_id", paymentDataMain.paymentData.order_id);//from response of step 3.
//            options.put("theme.color", "#3399cc");
//            options.put("currency", "INR");
//            options.put("amount", paymentDataMain.paymentData.amount);//pass amount in currency subunits
//            options.put("prefill.email", "razthemoster@gmail.com");
//            options.put("prefill.contact", paymentDataMain.paymentData.mobile);
//            options.put("payment_capture", "1");
//            JSONObject retryObj = new JSONObject();
//            retryObj.put("enabled", true);
//            retryObj.put("max_count", 4);
//            options.put("retry", retryObj);
//
//            checkout.open(this, options);
//
//        } catch(Exception e) {
//            Log.e("LogMsg", "Error in starting Razorpay Checkout", e);
//        }
    }

    @Override
    public void onPaymentVerificationSuccess() {

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onPaymentSuccess(String s) {
    }

    @Override
    public void onPaymentError(int i, String s) {
    }
}