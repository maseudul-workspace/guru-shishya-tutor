package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.VerifyPaymentInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.PlaceOrderInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.VerifyPaymentInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.PaymentDataMain;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.PaymentPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

public class PaymentPresenterImpl extends AbstractPresenter implements PaymentPresenter, PlaceOrderInteractor.Callback, VerifyPaymentInteractor.Callback {

    Context mContext;
    PaymentPresenter.View mView;

    public PaymentPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void placeOrder() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            PlaceOrderInteractorImpl placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            placeOrderInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void verifyPayment(String razorpayOrderId, String razorpayPaymentId, String razorpaySignature) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            VerifyPaymentInteractorImpl verifyPaymentInteractor = new VerifyPaymentInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, razorpayOrderId, razorpayPaymentId, razorpaySignature);
            verifyPaymentInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onPlaceOrderSuccess(PaymentDataMain paymentDataMain) {
        mView.loadData(paymentDataMain);
        mView.hideLoader();
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onVerifyPaymentSuccess() {
        mView.hideLoader();
        mView.onPaymentVerificationSuccess();
    }

    @Override
    public void onVerifyPaymentFail(String errorMsg) {
        mView.hideLoader();
    }
}
