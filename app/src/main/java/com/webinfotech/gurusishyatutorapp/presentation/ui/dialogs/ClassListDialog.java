package com.webinfotech.gurusishyatutorapp.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.Class;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.ClassesAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassListDialog implements ClassesAdapter.Callback {

    public interface Callback {
        void onClassSelected(int position);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.recycler_view_class)
    RecyclerView recyclerViewClassList;

    public ClassListDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.dialog_class_list, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }


    @Override
    public void onClassSelect(int position) {
        hideDialog();
        mCallback.onClassSelected(position);
    }

    public void setRecyclerViewClassList(Class[] classList) {
        ClassesAdapter adapter = new ClassesAdapter(mContext, classList, this);
        recyclerViewClassList.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewClassList.setAdapter(adapter);
        recyclerViewClassList.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }


}
