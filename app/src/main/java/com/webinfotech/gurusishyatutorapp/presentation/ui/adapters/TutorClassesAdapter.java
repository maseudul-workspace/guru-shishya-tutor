package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.TutorClass;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TutorClassesAdapter extends RecyclerView.Adapter<TutorClassesAdapter.ViewHolder> implements TutorSubjectsAdapter.Callback {

    @Override
    public void onSubjectDeleteClicked(int subjectId) {
        mCallback.onSubjectDeleteClicked(subjectId);
    }

    public interface Callback {
        void onClassDeleteClicked(int classId);
        void onSubjectDeleteClicked(int subjectId);
    }

    Context mContext;
    TutorClass[] classes;
    Callback mCallback;

    public TutorClassesAdapter(Context mContext, TutorClass[] classes, Callback mCallback) {
        this.mContext = mContext;
        this.classes = classes;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_class, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewClass.setText("Class " + classes[position].name);
        TutorSubjectsAdapter adapter = new TutorSubjectsAdapter(mContext, classes[position].subjects, this);
        holder.recyclerViewSubject.setAdapter(adapter);
        holder.recyclerViewSubject.setLayoutManager(new LinearLayoutManager(mContext));
        holder.imgViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation Message");
                builder.setMessage("You are about to remove a class. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("Yes"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onClassDeleteClicked(classes[position].id);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("No"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return classes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_class)
        TextView txtViewClass;
        @BindView(R.id.img_view_delete)
        ImageView imgViewDelete;
        @BindView(R.id.recycler_view_subject)
        RecyclerView recyclerViewSubject;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
