package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.BookingListPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.BookingListPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.BookingListRecyclerViewAdapter;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingListActivity extends AppCompatActivity implements BookingListPresenter.View {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.recycler_view_booking_list)
    RecyclerView recyclerViewBookingList;
    @BindView(R.id.pagination_progressbar)
    View paginationProgressbar;

    BookingListPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    LinearLayoutManager layoutManager;

    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    int orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Booking List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        setBottomNavigationView();
        initialsiePresenter();
        orderId = getIntent().getIntExtra("orderId", 0);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialsiePresenter() {
        mPresenter = new BookingListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }


    @Override
    public void loadAdapter(BookingListRecyclerViewAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewBookingList.setLayoutManager(layoutManager);
        recyclerViewBookingList.setAdapter(adapter);
        recyclerViewBookingList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        paginationProgressbar.setVisibility(View.VISIBLE);
                        paginationProgressbar.requestFocus();
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchBookingHistory(pageNo,"");
                    }
                    else
                    {
                        paginationProgressbar.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        pageNo = 1;
        totalPage = 1;
        isScrolling = false;
        mPresenter.fetchBookingHistory(pageNo, "refresh");
    }

    @Override
    public void goToBookingListDetails(int orderId) {
        Intent intent = new Intent(this, BookingDetailsActivity.class);
        intent.putExtra("orderId", orderId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void hidePaginationLoader() {
        paginationProgressbar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id. nav_home:
                        goToHomeActivity();
                        break;
                    case R.id.nav_time_slot:
                        goToTimeSlotsActivity();
                        break;
                    case R.id.nav_profile:
                        goToUserProfile();
                        break;
                    case R.id.nav_subject:
                        goToSubjectListActivity();
                        break;
                }
                return false;
            }
        });
    }


    private void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void goToSubjectListActivity() {
        Intent intent = new Intent(this, SubjectsActivity.class);
        startActivity(intent);
    }

    private void goToTimeSlotsActivity() {
        Intent intent = new Intent(this, TimeSlotActivity.class);
        startActivity(intent);
    }

    private void goToUserProfile() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

}