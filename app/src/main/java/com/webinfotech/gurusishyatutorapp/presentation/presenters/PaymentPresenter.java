package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.domain.models.PaymentDataMain;

public interface PaymentPresenter {
    void placeOrder();
    void verifyPayment( String razorpayOrderId,
                        String razorpayPaymentId,
                        String razorpaySignature);
    interface View {
        void loadData(PaymentDataMain paymentDataMain);
        void onPaymentVerificationSuccess();
        void showLoader();
        void hideLoader();
    }
}
