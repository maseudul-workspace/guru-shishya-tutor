package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.domain.models.BookingDetailsData;

public interface BookingDetailsPresenter {
    void fetchBookingDetails(int orderId);
    void acceptBooking(int orderId);
    void rejectBooking(int orderId);
    interface View{
        void loadBookingDetails(BookingDetailsData bookingDetailsData);
        void showLoader();
        void hideLoader();
    }
}
