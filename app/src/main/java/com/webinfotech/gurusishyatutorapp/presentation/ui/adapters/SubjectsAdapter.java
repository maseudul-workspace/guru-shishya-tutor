package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.Subject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectsAdapter extends RecyclerView.Adapter<SubjectsAdapter.ViewHolder> {

    public interface Callback {
        void onSubjectSelect(int position);
    }

    Context mContext;
    Subject[] subjects;
    Callback mCallback;

    public SubjectsAdapter(Context mContext, Subject[] subjects, Callback mCallback) {
        this.mContext = mContext;
        this.subjects = subjects;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_text, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (subjects[position].stream == null) {
            holder.txtViewText.setText(subjects[position].name);
        } else {
            holder.txtViewText.setText(subjects[position].name + " | " + subjects[position].stream);
        }
        holder.txtViewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSubjectSelect(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjects.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_text)
        TextView txtViewText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
