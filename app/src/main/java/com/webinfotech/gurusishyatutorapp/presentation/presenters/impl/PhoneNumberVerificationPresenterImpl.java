package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.SendOtpInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.VerifyOtpInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.SendOtpInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.VerifyOtpInteractorImpl;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.PhoneNumberVerificationPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class PhoneNumberVerificationPresenterImpl extends AbstractPresenter implements  PhoneNumberVerificationPresenter,
                                                                                        SendOtpInteractor.Callback,
                                                                                        VerifyOtpInteractor.Callback
{

    Context mContext;
    PhoneNumberVerificationPresenter.View mView;

    public PhoneNumberVerificationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phoneNo) {
        SendOtpInteractorImpl sendOtpInteractor = new SendOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phoneNo);
        sendOtpInteractor.execute();
    }

    @Override
    public void verifyOtp(String phoneNo, String otp) {
        VerifyOtpInteractorImpl verifyOtpInteractor = new VerifyOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phoneNo, otp);
        verifyOtpInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onSendOtpSuccess() {
        mView.hideLoader();
        mView.onSendOtpSuccess();
        Toasty.success(mContext, "Otp Sent Successfully").show();
    }

    @Override
    public void onSendOtpFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOtpVerifySuccess() {
        mView.hideLoader();
        mView.onOtpVerifySucces();
    }

    @Override
    public void onOtpVerifyFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

}
