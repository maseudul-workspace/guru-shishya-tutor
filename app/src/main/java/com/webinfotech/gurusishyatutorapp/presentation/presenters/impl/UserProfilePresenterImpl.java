package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchUserDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.UpdateProfileInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.FetchUserDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.UpdateProfileInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.UserProfilePresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class UserProfilePresenterImpl extends AbstractPresenter implements  UserProfilePresenter,
                                                                            UpdateProfileInteractor.Callback,
                                                                            FetchUserDetailsInteractor.Callback
{

    Context mContext;
    UserProfilePresenter.View mView;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchUserDetailsInteractorImpl fetchUserDetailsInteractor = new FetchUserDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchUserDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateProfile(String state, String city, String pin, double latitude, double longitude, String address, String qualification, String experience, String youtubeLink, String document, String certificate, String profileImage) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            UpdateProfileInteractorImpl updateProfileInteractor = new UpdateProfileInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, userInfo.userId, state, city, pin, latitude, longitude, address, qualification, experience, youtubeLink, document, certificate, profileImage);
            updateProfileInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onProfileUpdateSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Successfully Updated").show();
        mView.onUpdateProfileSuccess();
    }

    @Override
    public void onProfileUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onGettingUserDetailsSuccess(UserInfo userInfo) {
        mView.hideLoader();
        mView.loadUserDetails(userInfo.userDetails);
    }

    @Override
    public void onGettingUserDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }
}
