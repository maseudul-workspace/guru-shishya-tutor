package com.webinfotech.gurusishyatutorapp.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.executors.impl.ThreadExecutor;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.WalletHistoryListPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.impl.WalletHistoryListPresenterImpl;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.WalletHistoryListRecyclerViewAdapter;
import com.webinfotech.gurusishyatutorapp.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHistoryActivity extends AppCompatActivity implements WalletHistoryListPresenter.View {

    @BindView(R.id.text_view_total_balance)
    TextView textViewTotalBalance;
    @BindView(R.id.relative_layout_no_history)
    RelativeLayout relativeLayoutNoHistory;
    @BindView(R.id.recycler_view_wallet_History_list)
    RecyclerView recyclerViewWalletHistoryList;

    WalletHistoryListRecyclerViewAdapter adapter;
    WalletHistoryListPresenterImpl mPresenter;
    LinearLayoutManager layoutManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Wallet History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialsiePresenter();
        AndroidApplication androidApplication = (AndroidApplication) this.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        mPresenter.fetchWalletHistory(userInfo.userId);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialsiePresenter() {
        mPresenter = new WalletHistoryListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void loadWalletHistory(WalletHistoryData walletHistoryData) {
        textViewTotalBalance.setText("₹ "+walletHistoryData.amount);
        if(walletHistoryData.walletHistories.length == 0)
        {
            relativeLayoutNoHistory.setVisibility(View.VISIBLE);
            recyclerViewWalletHistoryList.setVisibility(View.GONE);
        }
        adapter = new WalletHistoryListRecyclerViewAdapter(this, walletHistoryData.walletHistories);
        layoutManager = new LinearLayoutManager(this);
        recyclerViewWalletHistoryList.setLayoutManager(layoutManager);
        recyclerViewWalletHistoryList.setAdapter(adapter);
    }

    @Override
    public void showLoader() {
       progressDialog.show();
    }

    @Override
    public void hideLoader() {
       progressDialog.hide();
    }
}