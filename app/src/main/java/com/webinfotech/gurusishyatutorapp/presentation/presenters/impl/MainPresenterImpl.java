package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.gurusishyatutorapp.AndroidApplication;
import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchJobDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.FetchUserDetailsInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.SendAppIdInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.TutionListInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.WalletHistoryInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.WithdrawRequestInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.FetchJobDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.FetchUserDetailsInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.SendAppIdInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.TutionListInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.WalletHistoryInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.WithdrawRequestInteractorImpl;
import com.webinfotech.gurusishyatutorapp.domain.models.JobDetails;
import com.webinfotech.gurusishyatutorapp.domain.models.TutionList;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletHistoryData;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.MainPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutionListRecyclerViewAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TutorClassesAdapter;
import com.webinfotech.gurusishyatutorapp.presentation.ui.adapters.TimeSlotsAdapter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchJobDetailsInteractor.Callback,
                                                                    TutorClassesAdapter.Callback,
                                                                    TimeSlotsAdapter.Callback,
                                                                    FetchUserDetailsInteractor.Callback,
                                                                    WithdrawRequestInteractor.Callback,
                                                                    WalletHistoryInteractor.Callback,
                                                                    TutionListInteractor.Callback,
                                                                    TutionListRecyclerViewAdapter.Callback,
                                                                    SendAppIdInteractor.Callback
{

    Context mContext;
    MainPresenter.View mView;
    TutionList[] newTutionLists = null;
    TutionListRecyclerViewAdapter adapter;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, MainPresenter.View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchUserDetailsInteractorImpl fetchUserDetailsInteractor = new FetchUserDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchUserDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchJobDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchJobDetailsInteractorImpl fetchJobDetailsInteractor = new FetchJobDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchJobDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchWalletHistory(int tutor_id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        WalletHistoryInteractorImpl walletHistoryInteractor = new WalletHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, tutor_id);
        walletHistoryInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchTutionList(int page, String refresh) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (refresh.equals("refresh")) {
            newTutionLists = null;
        }
        if (userInfo != null) {
            TutionListInteractorImpl tutionListInteractorImpl = new TutionListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, page);
            tutionListInteractorImpl.execute();
        }
        mView.showLoader();
    }

    @Override
    public void withdrawRequest(String amount) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        WithdrawRequestInteractorImpl withdrawRequestInteractor = new WithdrawRequestInteractorImpl(mExecutor,mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, amount, userInfo.userId);
        withdrawRequestInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void sendAppId(String app_id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            SendAppIdInteractorImpl sendAppIdInteractor = new SendAppIdInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, app_id);
            sendAppIdInteractor.execute();
        }
    }

    @Override
    public void onGettingJobDetailsSuccess(JobDetails jobDetails) {
        TutorClassesAdapter adapter = new TutorClassesAdapter(mContext, jobDetails.tutorClasses, this);
        TimeSlotsAdapter timeSlotsAdapter = new TimeSlotsAdapter(mContext, jobDetails.timeSlots, this);
        mView.loadClasses(adapter, timeSlotsAdapter);
    }

    @Override
    public void onGettingJobDetailsFail(String errorMsg, int loginError) {

    }


    @Override
    public void onClassDeleteClicked(int classId) {

    }

    @Override
    public void onSubjectDeleteClicked(int subjectId) {

    }

    @Override
    public void onDeleteTimeSlotClicked(int timeSlotId) {

    }

    @Override
    public void onGettingUserDetailsSuccess(UserInfo userInfo) {
        mView.loadUserDetails(userInfo);
    }

    @Override
    public void onGettingUserDetailsFail(String errorMsg, int loginError) {

    }

    @Override
    public void onWithdrawRequestSentSuccess() {
        mView.hideLoader();
        mView.onWithdrawRequestSuccess();
    }

    @Override
    public void onWithdrawRequestSentFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT ).show();
    }

    @Override
    public void onGettingWalletHistorySuccess(WalletHistoryData walletHistoryData) {
        mView.hideLoader();
        mView.loadWalletHistory(walletHistoryData);
    }

    @Override
    public void onGettingWalletHistoryFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingTutionListSuccess(TutionList[] tutionLists, int totalPage) {
        TutionList[] tempTutionList;
        tempTutionList = newTutionLists;
        try {
            int len1 = tempTutionList.length;
            int len2 = tutionLists.length;
            newTutionLists = new TutionList[len1 + len2];
            System.arraycopy(tempTutionList, 0, newTutionLists, 0, len1);
            System.arraycopy(tutionLists, 0, newTutionLists, len1, len2);
            adapter.updateData(newTutionLists);
        }catch (NullPointerException e){
            newTutionLists = tutionLists;
            adapter = new TutionListRecyclerViewAdapter(mContext, tutionLists, this);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
        mView.hidePaginationLoader();

    }

    @Override
    public void onGettingTutionListFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onTutionClicked(int OrderId) {
        mView.goToTutionListDetails(OrderId);
    }

    @Override
    public void onSendAppIdSuccess() {
    }

    @Override
    public void onSendAppIdFail(String errorMsg, int loginError) {
    }
}
