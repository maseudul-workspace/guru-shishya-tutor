package com.webinfotech.gurusishyatutorapp.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.webinfotech.gurusishyatutorapp.R;
import com.webinfotech.gurusishyatutorapp.domain.models.WalletList;
import com.webinfotech.gurusishyatutorapp.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletListAdapter extends RecyclerView.Adapter<WalletListAdapter.ViewHolder> {

    public interface Callback {
        void onWalletSelected(int position);
    }

    Context mContext;
    ArrayList<WalletList> walletLists;
    Callback mCallback;

    public WalletListAdapter(Context mContext, ArrayList<WalletList> walletLists, Callback mCallback) {
        this.mContext = mContext;
        this.walletLists = walletLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_wallet_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (walletLists.get(position).isSelected) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }
        switch (walletLists.get(position).walletCode) {
            case "mobikwik":
                GlideHelper.setImageView(mContext, holder.imgViewWallet, "https://d1oq1nzr46kpxm.cloudfront.net/assets/mobikwik_logo_B-5e2d8977879fcf758f11da71704c05f34cb4490949abb87ed8ff009437a5f017.png");
                break;
            case "payzapp":
                GlideHelper.setImageView(mContext, holder.imgViewWallet, "https://i.pinimg.com/originals/3e/88/1f/3e881fe5856ad85fa8c91f5095d1584a.jpg");
                break;
            case "airtelmoney":
                GlideHelper.setImageView(mContext, holder.imgViewWallet, "https://amref.org/wp-content/uploads/2017/10/airtel-money.png");
                break;
            case "freecharge":
                GlideHelper.setImageView(mContext, holder.imgViewWallet, "https://upload.wikimedia.org/wikipedia/commons/9/90/FreeCharge_Logo.png");
                break;
            case "jiomoney":
                GlideHelper.setImageView(mContext, holder.imgViewWallet, "https://happysale.in/hs/img/m/jiomoney.jpeg");
                break;
        }

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onWalletSelected(position);
            }
        });

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.radioButton.setChecked(true);
                mCallback.onWalletSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return walletLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_wallet)
        ImageView imgViewWallet;
        @BindView(R.id.radio_btn)
        RadioButton radioButton;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setData(ArrayList<WalletList> walletLists) {
        this.walletLists = walletLists;
        notifyDataSetChanged();
    }

}
