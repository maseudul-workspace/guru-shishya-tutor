package com.webinfotech.gurusishyatutorapp.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.gurusishyatutorapp.domain.executors.Executor;
import com.webinfotech.gurusishyatutorapp.domain.executors.MainThread;
import com.webinfotech.gurusishyatutorapp.domain.interactors.ForgetPassowordInteractor;
import com.webinfotech.gurusishyatutorapp.domain.interactors.impl.ForgetPasswordInteractorImpl;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.gurusishyatutorapp.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.gurusishyatutorapp.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, ForgetPassowordInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;

    public ForgetPasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void requestPasswordChange(String otp, String mobile, String password) {
        ForgetPasswordInteractorImpl forgetPasswordInteractorImpl = new ForgetPasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, otp, mobile, password, password);
        forgetPasswordInteractorImpl.execute();
    }

    @Override
    public void onForgetPasswordSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Password Changed").show();
        mView.onPasswordRequestSuccess();
    }

    @Override
    public void pnForgetPasswordFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
