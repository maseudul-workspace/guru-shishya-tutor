package com.webinfotech.gurusishyatutorapp.presentation.presenters;

import com.webinfotech.gurusishyatutorapp.domain.models.Class;

public interface AddSubjectPresenter {
    void fetchJobDetails();
    void addJob(int classId, int subjectId);
    interface View {
        void loadData(Class[] classes);
        void onJobAddSuccess();
        void showLoader();
        void hideLoader();
    }
}
