package com.webinfotech.gurusishyatutorapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.webinfotech.gurusishyatutorapp.domain.models.UserInfo;


public class AndroidApplication extends Application {

    UserInfo userInfo;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "ANY MEDS", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("USER", new Gson().toJson(userInfo));
        } else {
            editor.putString("USER", "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "ANY MEDS", Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString("USER","");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

}
